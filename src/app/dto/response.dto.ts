export interface RequestResponseDto<T> {
  success: boolean;
  data: T;
}
