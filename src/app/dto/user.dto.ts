export interface UsernameResponseDto {
  timestamp: string;
  username: string;
}

export interface PasswordResponseDto {
  message: string;
  token: string;
}
