export interface RecipeDto {
  _id: string;
  name: string;
  prepDuration: string;
  cookDuration: string;
  servings: string;
  ingredients: IngredientDto[];
  directions: DirectionDto[];
  userId: string;
}

export interface IngredientDto {
  name: string;
  unit?: string;
  amount: number;
  id: number;
}

export interface DirectionDto {
  text: string;
  duration: string;
  id: number;
}

export interface AddRecipeDto {
  msg: string;
}

export interface DeleteRecipeDto {
  msg: string;
}

export interface UpdateRecipeDto {
  msg: string;
}
