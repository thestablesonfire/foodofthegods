import { IngredientDto } from "./recipe.dto";

export interface IngredientListContainerDto {
  _id: string;
  userId: string;
  ingredientList: IngredientListDto;
}

export interface IngredientListDto {
  items: IngredientListItemDto[];
  lastModified: string;
}

export interface IngredientListItemDto {
  ingredient: IngredientDto;
  completed: boolean;
}

export interface SaveIngredientListResponse {
  msg: string;
}
