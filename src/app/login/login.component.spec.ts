import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Router } from "@angular/router";
import { of, throwError } from "rxjs";
import { MockAuthService } from "../mocks/mock-auth.service";
import { MockRouter } from "../mocks/mock-router";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { LoginComponent } from "./login.component";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: MockAuthService;
  let router: MockRouter;
  let authSpy: jasmine.Spy;
  const username = "testuser";
  const tokenVal = "123TOKEN123";
  const hashedUsername = "5d9c68c6c50ed3d02a2fcf54f63993b6";
  const password = "test";
  const hashedPassword = "098f6bcd4621d373cade4e832627b4f6";

  beforeEach(waitForAsync(() => {
    authService = new MockAuthService();
    router = new MockRouter();
    authSpy = authService.getToken.and.returnValue(of(tokenVal));

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [BrowserAnimationsModule, FormsModule],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: Router, useValue: router },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should reset the login form on cancel", () => {
    component.username = "123username123";
    component.password = "123password123";

    component.onLoginCancel();

    expect(component.username).toEqual("");
    expect(component.password).toEqual("");
  });

  it("should return and set token correctly", () => {
    const routerSpy = router.navigate;
    component.username = username;
    component.password = password;

    component.submitLogin();

    expect(authSpy).toHaveBeenCalledTimes(1);
    expect(authSpy).toHaveBeenCalledWith(hashedUsername, hashedPassword);
    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["recipes"]);
  });

  it("should handle not receiving a token", () => {
    authSpy.and.returnValue(of(null));
    component.username = username;
    component.password = password;

    component.submitLogin();

    expect(component.authError).toBe(true);
  });

  it("should handle the promise rejection", () => {
    authSpy.and.returnValue(throwError(() => "error"));
    component.username = username;
    component.password = password;

    component.submitLogin();

    expect(component.authError).toBe(true);
  });
});
