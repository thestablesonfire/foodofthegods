import { Component } from "@angular/core";
import { Router } from "@angular/router";
import md5 from "md5";
import { pbkdf2 } from "pbkdf2";
import { take } from "rxjs";
import { AuthService } from "../recipe-book/services/auth/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent {
  public authError: boolean;
  public md5: typeof md5;
  public password: string;
  public pbkdf2: typeof pbkdf2;
  public username: string;

  constructor(private authService: AuthService, private router: Router) {
    this.md5 = require("md5");
    this.pbkdf2 = require("pbkdf2");
    this.authError = false;
  }

  public onLoginCancel(): void {
    this.username = "";
    this.password = "";
  }

  public submitLogin(): void {
    const usernameHash = this.md5(this.username);
    const passwordHash = this.md5(this.password);

    this.authService
      .getToken(usernameHash, passwordHash)
      .pipe(take(1))
      .subscribe({
        next: (token) => {
          if (token) {
            this.router.navigate(["recipes"]);
          } else {
            this.authError = true;
          }
        },
        error: () => {
          this.authError = true;
        },
      });
  }
}
