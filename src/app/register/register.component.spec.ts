import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormBuilder,
} from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { MockAuthService } from "../mocks/mock-auth.service";
import { MockRouter } from "../mocks/mock-router";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { RegisterComponent } from "./register.component";
import { RegisterService } from "./register.service";

describe("RegisterComponent", () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let router: MockRouter;
  let registerService: RegisterService;

  beforeEach(waitForAsync(() => {
    router = new MockRouter();

    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [BrowserAnimationsModule, FormsModule, ReactiveFormsModule],
      providers: [
        UntypedFormBuilder,
        { provide: AuthService, useClass: MockAuthService },
        {
          provide: RegisterService,
          useValue: { sendRegistrationRequest: () => null },
        },
        { provide: Router, useValue: router },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    registerService = TestBed.inject(RegisterService);

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should test the registration send", () => {
    const passSpy = spyOn(AuthService, "hashPassword").and.returnValue("321");
    const sendRegSpy = spyOn(
      registerService,
      "sendRegistrationRequest"
    ).and.returnValue(of({ msg: "ok" }));
    const routerSpy = router.navigate;
    component.registerForm.get("username").setValue("123");
    component.registerForm.get("passwordMatch").get("password").setValue("123");

    component.submitRegister();

    expect(passSpy).toHaveBeenCalledTimes(1);
    expect(sendRegSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["/register/thanks"]);
  });

  it("should test the registration send", () => {
    const routerSpy = router.navigate;

    component.onCancel();

    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["/"]);
  });
});
