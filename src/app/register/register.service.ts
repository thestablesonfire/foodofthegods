import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { RegistrationResponseDto } from "../dto/registration-response.dto";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { RegistrationInfo } from "./registration-info";

@Injectable()
export class RegisterService {
  private baseURL: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.baseURL = environment.baseUrl + "/mail";
  }

  public sendRegistrationRequest(
    regInfo: RegistrationInfo
  ): Observable<RegistrationResponseDto> {
    return this.http.post<RegistrationResponseDto>(
      this.baseURL,
      regInfo,
      this.authService.getConfig()
    );
  }
}
