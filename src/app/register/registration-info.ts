export interface RegistrationInfo {
  emailAddress: string;
  username: string;
  password: string;
  timestamp: string;
}
