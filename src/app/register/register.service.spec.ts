import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { firstValueFrom, of } from "rxjs";
import { MockAuthService } from "../mocks/mock-auth.service";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { RegisterService } from "./register.service";
import { RegistrationInfo } from "./registration-info";

describe("RegisterService", () => {
  let service: RegisterService;
  let http: HttpClient;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        RegisterService,
      ],
      imports: [HttpClientTestingModule],
    });
  });

  beforeEach(() => {
    service = TestBed.inject(RegisterService);
    authService = TestBed.inject(AuthService);
    http = TestBed.inject(HttpClient);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should send the registration request", async () => {
    const response = { msg: "ok" };
    const httpSpy = spyOn(http, "post").and.returnValue(of(response));
    const regInfo: RegistrationInfo = {
      emailAddress: "123",
      username: "123",
      password: "123",
      timestamp: "123",
    };

    const res = await firstValueFrom(service.sendRegistrationRequest(regInfo));

    expect(res).toEqual(response);
    expect(httpSpy).toHaveBeenCalledTimes(1);
    expect(httpSpy).toHaveBeenCalledWith(
      "https://theunderempire.com:3000/mail",
      regInfo,
      authService.getConfig()
    );
  });
});
