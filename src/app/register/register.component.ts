import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  UntypedFormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import md5 from "md5";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { RegisterService } from "./register.service";
import { RegistrationInfo } from "./registration-info";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.registerForm = this.getRegisterForm();
  }

  public onCancel(): void {
    this.router.navigate(["/"]);
  }

  public shouldShowFieldValidation(control: AbstractControl): boolean {
    return control.errors && !control.pristine;
  }

  public submitRegister(): void {
    let password = md5(
      this.registerForm.get("passwordMatch").get("password").value
    );
    const username = md5(this.registerForm.get("username").value);
    const timestamp = new Date().toString();

    password = AuthService.hashPassword(timestamp, password);
    const newUser: RegistrationInfo = {
      emailAddress: this.registerForm.get("email").value,
      username,
      password,
      timestamp,
    };

    this.registerService.sendRegistrationRequest(newUser).subscribe(() => {
      this.router.navigate(["/register/thanks"]);
    });
  }

  private getRegisterForm(): UntypedFormGroup {
    return this.formBuilder.group({
      email: ["", [Validators.email, Validators.required]],
      username: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(30),
        ],
      ],
      passwordMatch: this.formBuilder.group(
        {
          password: ["", [Validators.required]],
          passwordConfirm: ["", [Validators.required]],
        },
        { validator: this.passwordMatch }
      ),
    });
  }

  private passwordMatch(c: AbstractControl): Promise<{ invalid: boolean }> {
    if (c.get("password").value !== c.get("passwordConfirm").value) {
      return Promise.resolve({ invalid: true });
    }
  }
}
