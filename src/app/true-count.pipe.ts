import { Pipe, PipeTransform } from "@angular/core";

import { IngredientListItem } from "./ingredient-list/ingredient-list-item/ingredient-list-item";

@Pipe({
  name: "trueCount",
})
export class TrueCountPipe implements PipeTransform {
  public transform(value: IngredientListItem[]): number {
    return value.reduce((sum, listItem) => {
      return (sum += listItem.completed ? 0 : 1);
    }, 0);
  }
}
