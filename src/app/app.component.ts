import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

import pckg from "../../package.json";
import { AuthService } from "./recipe-book/services/auth/auth.service";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-root",
  styleUrls: ["../styles.scss", "../forms.scss", "./app.component.scss"],
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  public isRegistrationPage: boolean;
  public title: string;
  public version: string;

  constructor(private authService: AuthService, private router: Router) {
    this.isRegistrationPage = false;
    this.title = "Food of the Gods";
    this.version = pckg.version;
  }

  public goHome(): void {
    this.router.navigate(["/"]);
  }

  public isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  public logOut(): void {
    this.authService.logOut();
  }

  public ngOnInit(): void {
    // Every time you navigate somewhere, we want to check
    // if it is the reg page so we can change a few display items
    this.router.events.subscribe((e: NavigationEnd) => {
      if (e instanceof NavigationEnd) {
        this.isRegistrationPage = e.url.includes("register");
      }
    });
  }

  public goToRegistration(): void {
    this.router.navigate(["/register"]);
  }
}
