import { DragDropModule } from "@angular/cdk/drag-drop";
import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

import { AppComponent } from "./app.component";
import { CompareDirective } from "./compare.directive";
import { ConfirmationDialogComponent } from "./confirmation-dialog/confirmation-dialog.component";
import { IngredientListItemComponent } from "./ingredient-list/ingredient-list-item/ingredient-list-item.component";
import { IngredientListComponent } from "./ingredient-list/ingredient-list.component";
import { IngredientListService } from "./ingredient-list/ingredient-list.service";
import { LoginComponent } from "./login/login.component";
import { RecipeFilterComponent } from "./recipe-book/recipe-filter/recipe-filter.component";
import { RecipeFormComponent } from "./recipe-book/recipe-form/recipe-form.component";
import { RecipeListItemComponent } from "./recipe-book/recipe-list/recipe-list-item/recipe-list-item.component";
import { RecipeListComponent } from "./recipe-book/recipe-list/recipe-list.component";
import { RecipeViewerComponent } from "./recipe-book/recipe-viewer/recipe-viewer.component";
import { RecipeFactory } from "./recipe-book/recipe/recipe-factory";
import {
  AuthGuardService,
  isAuthenticated,
} from "./recipe-book/services/auth/auth-guard.service";
import { AuthService } from "./recipe-book/services/auth/auth.service";
import { HttpResponseInterceptor } from "./recipe-book/services/auth/http-response.interceptor";
import { EditRecipeService } from "./recipe-book/services/edit-recipe/edit-recipe.service";
import { RecipesService } from "./recipe-book/services/recipes/recipes.service";
import { RegisterThanksComponent } from "./register-thanks/register-thanks.component";
import { RegisterComponent } from "./register/register.component";
import { RegisterService } from "./register/register.service";
import { TrueCountPipe } from "./true-count.pipe";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "recipes",
  },
  {
    canActivate: [isAuthenticated],
    component: RecipeListComponent,
    path: "recipes",
  },
  {
    canActivate: [isAuthenticated],
    component: RecipeViewerComponent,
    path: "recipes/recipe/:id",
  },
  {
    component: RecipeViewerComponent,
    path: "recipes/share/:shareId",
  },
  {
    canActivate: [isAuthenticated],
    component: RecipeFormComponent,
    path: "recipes/add",
  },
  {
    canActivate: [isAuthenticated],
    component: RecipeFormComponent,
    path: "recipes/edit/:id",
  },
  {
    canActivate: [isAuthenticated],
    component: IngredientListComponent,
    path: "ingredients",
  },
  {
    component: LoginComponent,
    path: "login",
  },
  {
    component: RegisterComponent,
    path: "register",
  },
  {
    component: RegisterThanksComponent,
    path: "register/thanks",
  },
  {
    path: "**",
    redirectTo: "recipes",
  },
];

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    CompareDirective,
    ConfirmationDialogComponent,
    IngredientListComponent,
    IngredientListItemComponent,
    LoginComponent,
    RecipeFilterComponent,
    RecipeFormComponent,
    RecipeListComponent,
    RecipeListItemComponent,
    RecipeViewerComponent,
    RegisterComponent,
    RegisterThanksComponent,
    TrueCountPipe,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    DragDropModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    AuthGuardService,
    AuthService,
    CookieService,
    EditRecipeService,
    IngredientListService,
    RecipeFactory,
    RecipesService,
    RegisterService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpResponseInterceptor,
      multi: true,
    },
  ],
})
export class AppModule {}
