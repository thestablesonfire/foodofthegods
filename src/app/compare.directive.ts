import { Attribute, Directive } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
  providers: [
    { provide: NG_VALIDATORS, useExisting: CompareDirective, multi: true },
  ],
  selector: "[appCompare]",
})
export class CompareDirective implements Validator {
  constructor(@Attribute("appCompare") public comparer: string) {}

  public validate(c: AbstractControl): { [key: string]: any } {
    const e: any = c.root.get(this.comparer);
    const password = e.controls.password.value;
    const passwordConfirm = c.value;

    if (e.controls.password && password !== passwordConfirm) {
      return { compare: true };
    }

    return null;
  }
}
