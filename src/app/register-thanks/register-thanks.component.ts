import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-register-thanks",
  templateUrl: "./register-thanks.component.html",
})
export class RegisterThanksComponent {
  constructor(private router: Router) {}

  public goHome(): void {
    this.router.navigate(["/"]);
  }
}
