import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { Router } from "@angular/router";

import { MockRouter } from "../mocks/mock-router";
import { RegisterThanksComponent } from "./register-thanks.component";

describe("RegisterThanksComponent", () => {
  let component: RegisterThanksComponent;
  let fixture: ComponentFixture<RegisterThanksComponent>;
  let router: MockRouter;

  beforeEach(waitForAsync(() => {
    router = new MockRouter();

    TestBed.configureTestingModule({
      declarations: [RegisterThanksComponent],
      providers: [{ provide: Router, useValue: router }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should test navigate", () => {
    const routerSpy = router.navigate;

    component.goHome();

    expect(routerSpy).toHaveBeenCalledWith(["/"]);
  });
});
