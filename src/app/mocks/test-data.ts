import { IngredientListContainerDto } from "../dto/ingredient-list.dto";
import { RecipeDto } from "../dto/recipe.dto";
import { Direction } from "../recipe-book/recipe/direction/direction";
import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { Recipe } from "../recipe-book/recipe/recipe";

export class TestData {
  public static getIngredientListResponse(): IngredientListContainerDto[] {
    return [
      {
        userId: "userId",
        _id: "5a3ab88bc7f8a8023e33fa51",
        ingredientList: {
          items: [
            {
              completed: false,
              ingredient: {
                amount: 0.75,
                id: 1509136202120,
                name: "Salt",
                unit: "tsp.",
              },
            },
            {
              completed: false,
              ingredient: {
                amount: 0.25,
                id: 1509136202758,
                name: "Black Pepper",
                unit: "tsp.",
              },
            },
            {
              completed: false,
              ingredient: {
                amount: 0.25,
                id: 1509136203940,
                name: "Cayenne Pepper",
                unit: "tsp.",
              },
            },
          ],
          lastModified: "2020-09-28T18:25:37.487Z",
        },
      },
    ];
  }

  public static getRecipesResponse(): RecipeDto[] {
    return [
      {
        _id: "59f39803cf794009d3783f63",
        userId: "sdf",
        cookDuration: "20 min.",
        directions: [
          {
            duration: "5 min.",
            id: 1509136152414,
            text:
              "In a small bowl, mix together salt, black pepper, cayenne, paprika, .25 teaspoon" +
              "garlic powder, onion powder, thyme and parsley. Sprinkle spice mixture generously on both" +
              "sides of chicken breasts.",
          },
          {
            duration: "20 min.",
            id: 1509136375447,
            text:
              "Heat butter and olive oil in a large heavy skillet over medium heat. Saute chicken" +
              "until golden brown, about 6 minutes on each side. Sprinkle with 2 teaspoons garlic powder" +
              "and lime juice. Cook 5 minutes more, stirring frequently to coat evenly with sauce.",
          },
        ],
        ingredients: [
          {
            amount: 0.75,
            id: 1509136202120,
            name: "salt",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509136202758,
            name: "Black Pepper",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509136203940,
            name: "Cayenne Pepper",
            unit: "t",
          },
          {
            amount: 0.125,
            id: 1509136204544,
            name: "Paprika",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509136205541,
            name: "Garlic Powder",
            unit: "t",
          },
          {
            amount: 0.125,
            id: 1509136206435,
            name: "Onion Powder",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509136207318,
            name: "Dried Thyme",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509136208412,
            name: "Dried Parsley",
            unit: "t",
          },
          {
            amount: 4,
            id: 1509136209416,
            name: "Boneless, skinless chicken breast halves",
          },
          {
            amount: 2,
            id: 1509136210333,
            name: "Butter",
            unit: "T",
          },
          {
            amount: 1,
            id: 1509136211269,
            name: "Olive Oil",
            unit: "t",
          },
          {
            amount: 2,
            id: 1509136330420,
            name: "Garlic Powder",
            unit: "t",
          },
          {
            amount: 3,
            id: 1509136331284,
            name: "Lime Juice",
            unit: "t",
          },
        ],
        name: "Spicy Garlic-Lime Chicken",
        prepDuration: "5 min.",
        servings: "4",
      },
      {
        _id: "59f77c6cd865453d9a12b8c0",
        userId: "userId",
        cookDuration: "20 min",
        directions: [
          {
            duration: "5. min",
            id: 1509391087542,
            text: "Preheat oven to 400 degrees F. Grease 12 muffin cups or line with paper liners.",
          },
          {
            duration: "15 min.",
            id: 1509391413599,
            text:
              "Whisk chocolate chips, brown sugar, flour, cocoa powder, baking soda, baking" +
              "powder, and salt together in a bowl. Whisk soy milk, oil, applesauce, eggs, vinegar, and" +
              "vanilla extract together in a separate bowl; stir into flour mixture until batter is just" +
              "combined. Spoon batter into prepared muffin cups until each cup is .75-full.",
          },
          {
            duration: "20 min.",
            id: 1509391450434,
            text:
              "Bake in the preheated oven until a toothpick inserted in the center of a muffin" +
              "comes out clean, 20 to 25 minutes.",
          },
        ],
        ingredients: [
          {
            amount: 1,
            id: 1509391087542,
            name: "Chocolate Chips",
            unit: "t",
          },
          {
            amount: 1,
            id: 1509391241133,
            name: "Brown Sugar",
            unit: "t",
          },
          {
            amount: 1,
            id: 1509391241926,
            name: "Flour",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509391242505,
            name: "Cocoa Powder",
            unit: "t",
          },
          {
            amount: 1,
            id: 1509391243669,
            name: "Baking Soda",
            unit: "t",
          },
          {
            amount: 1,
            id: 1509391244243,
            name: "Baking Powder",
            unit: "t",
          },
          {
            amount: 1 / 2,
            id: 1509391244791,
            name: "salt",
            unit: "t",
          },
          {
            amount: 1,
            id: 1509391245964,
            name: "Soy Milk",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509391246592,
            name: "Grapeseed Oil",
            unit: "t",
          },
          {
            amount: 0.25,
            id: 1509391247192,
            name: "Unsweetened Applesauce",
            unit: "t",
          },
          {
            amount: 2,
            id: 1509391248139,
            name: "Eggs",
            unit: "t",
          },
          {
            amount: 2,
            id: 1509391248794,
            name: "Apple Cider Vinegar",
            unit: "t",
          },
          {
            amount: 2,
            id: 1509391249383,
            name: "Vanilla Extract",
            unit: "t",
          },
        ],
        name: "Chocolate Morning Muffins",
        prepDuration: "20 min.",
        servings: "12",
      },
    ];
  }

  public static getTestRecipe(): Recipe {
    return new Recipe(
      "salt water",
      "10 min.",
      "10 min.",
      4,
      [
        new Ingredient("salt", "T", 1, 123),
        new Ingredient("water", "c", 2, 234),
      ],
      [
        new Direction("Put the salt in the water", "5 min.", 1),
        new Direction("Boil the water", "5 min.", 2),
      ],
      "usernamehash123",
      "607"
    );
  }
}
