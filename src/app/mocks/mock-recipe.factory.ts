import { TestData } from "./test-data";

export class MockRecipeFactory {
  public static buildRecipes = jasmine
    .createSpy("buildRecipes")
    .and.returnValue(TestData.getTestRecipe());
}
