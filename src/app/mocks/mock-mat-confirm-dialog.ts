export class MockMatConfirmDialog {
  public close = jasmine.createSpy("close");
  public open = jasmine.createSpy("open");
}
