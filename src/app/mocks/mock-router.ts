import { NavigationEnd } from "@angular/router";
import { BehaviorSubject } from "rxjs";

export class MockRouter {
  public events = new BehaviorSubject<NavigationEnd>(
    new NavigationEnd(123, "/recipes", "/recipes")
  );
  public navigate = jasmine.createSpy("navigate");
}
