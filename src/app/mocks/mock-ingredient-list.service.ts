import { EventEmitter } from "@angular/core";

import { IngredientList } from "../ingredient-list/ingredient-list";
import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";

export class MockIngredientListService {
  public ingredientsChangedEvent: EventEmitter<null>;

  constructor() {
    this.ingredientsChangedEvent = new EventEmitter();
  }

  public ingredientList: IngredientList = new IngredientList();

  public addIngredient = jasmine.createSpy("addIngredient");
  public addIngredients = jasmine.createSpy("addIngredients");
  public clearIngredients = jasmine.createSpy("clearIngredients");
  public getIngredientList = jasmine
    .createSpy("getIngredientList")
    .and.returnValue([
      new Ingredient("test1", "t", 1),
      new Ingredient("test2", "t", 2),
      new Ingredient("test3", "t", 3),
      new Ingredient("test4", "t", 4),
    ]);

  public removeIngredient = jasmine.createSpy("removeIngredient");
  public saveIngredientList = jasmine.createSpy("saveIngredientList");
}
