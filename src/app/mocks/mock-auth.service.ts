import { EventEmitter } from "@angular/core";
import { AuthConfig } from "../recipe-book/services/auth/auth-config";

export class MockAuthService {
  public tokenUpdatedEvent: EventEmitter<null>;
  public token = "123";

  constructor() {
    this.tokenUpdatedEvent = new EventEmitter();
  }

  public getConfig = jasmine
    .createSpy("getConfig")
    .and.callFake(() => new AuthConfig(this.token));

  public getCookie = jasmine.createSpy("getCookie");
  public getToken = jasmine.createSpy("getToken");
  public getUsername = jasmine
    .createSpy("getUsername")
    .and.returnValue("username");
  public isAuthenticated = jasmine
    .createSpy("isAuthenticated")
    .and.returnValue(true);

  public logOut = jasmine.createSpy("logOut");
  public setToken = jasmine.createSpy("setToken");
}
