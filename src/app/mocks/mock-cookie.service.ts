export class MockCookieService {
  public check = jasmine.createSpy("check");
  public get = jasmine.createSpy("get");
  public getAll = jasmine.createSpy("getAll");
  public set = jasmine.createSpy("set");
  public delete = jasmine.createSpy("delete");
  public deleteAll = jasmine.createSpy("deleteAll");
}
