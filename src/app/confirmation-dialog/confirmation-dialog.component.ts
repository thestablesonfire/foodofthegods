import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "app-confirmation-dialog",
  templateUrl: "./confirmation-dialog.component.html",
})
export class ConfirmationDialogComponent {
  @Output() public dialogClosed: EventEmitter<boolean>;
  @Input() public itemName: string;
  @Input() public showDialog: boolean;

  constructor() {
    this.dialogClosed = new EventEmitter<boolean>();
  }
}
