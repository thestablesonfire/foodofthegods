import { APP_BASE_HREF } from "@angular/common";
import { Component } from "@angular/core";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { Router, Routes } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

import { AppComponent } from "./app.component";
import { ConfirmationDialogComponent } from "./confirmation-dialog/confirmation-dialog.component";
import { IngredientListComponent } from "./ingredient-list/ingredient-list.component";
import { LoginComponent } from "./login/login.component";
import { MockAuthService } from "./mocks/mock-auth.service";
import { RecipeFormComponent } from "./recipe-book/recipe-form/recipe-form.component";
import { RecipeListComponent } from "./recipe-book/recipe-list/recipe-list.component";
import { RecipeViewerComponent } from "./recipe-book/recipe-viewer/recipe-viewer.component";
import { AuthService } from "./recipe-book/services/auth/auth.service";
import { RegisterThanksComponent } from "./register-thanks/register-thanks.component";
import { RegisterComponent } from "./register/register.component";
import { TrueCountPipe } from "./true-count.pipe";

describe("AppComponent", () => {
  let component: ComponentFixture<AppComponent>;
  let app: AppComponent;
  let authService: MockAuthService;
  let router: Router;
  let navigateSpy: jasmine.Spy;

  @Component({
    selector: "app-mock-component",
    template: ``,
  })
  class MockComponent {}

  beforeEach(waitForAsync(() => {
    authService = new MockAuthService();

    const routes: Routes = [
      {
        path: "",
        pathMatch: "full",
        redirectTo: "recipes",
      },
      {
        component: MockComponent,
        path: "recipes",
      },
      {
        component: MockComponent,
        path: "recipes/recipe/:id",
      },
      {
        component: MockComponent,
        path: "recipes/share/:shareId",
      },
      {
        component: MockComponent,
        path: "recipes/add",
      },
      {
        component: MockComponent,
        path: "recipes/edit/:id",
      },
      {
        component: MockComponent,
        path: "ingredients",
      },
      {
        component: MockComponent,
        path: "login",
      },
      {
        component: MockComponent,
        path: "register",
      },
      {
        component: MockComponent,
        path: "register/thanks",
      },
      {
        path: "**",
        redirectTo: "recipes",
      },
    ];

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ConfirmationDialogComponent,
        IngredientListComponent,
        LoginComponent,
        RecipeFormComponent,
        RecipeListComponent,
        RecipeViewerComponent,
        RegisterComponent,
        RegisterThanksComponent,
        TrueCountPipe,
      ],
      imports: [FormsModule, RouterTestingModule.withRoutes(routes)],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: APP_BASE_HREF, useValue: "/" },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.inject(Router);
    navigateSpy = spyOn(router, "navigate");
    component = TestBed.createComponent(AppComponent);
    app = component.debugElement.componentInstance;
    component.detectChanges();
  });

  it("should create the app", () => {
    expect(app).toBeTruthy();
    expect(app.title).toBe("Food of the Gods");
    expect(app.version).toBeTruthy();
    expect(typeof app.version).toBe("string");
  });

  it("should navigate home", () => {
    app.goHome();

    expect(router.navigate).toHaveBeenCalledOnceWith(["/"]);
  });

  it("should call the isAuthenticated method from the AuthService", () => {
    const expectedValue = true;
    authService.isAuthenticated.and.returnValue(expectedValue);

    expect(app.isAuthenticated()).toBe(expectedValue);
  });

  it("should call the logOut method from the AuthService", () => {
    app.logOut();

    expect(authService.logOut).toHaveBeenCalledTimes(1);
  });

  it("should pick up NavigationEnd event and set isRegistration page to true", async () => {
    navigateSpy.and.callThrough();
    await router.navigate(["/register"]);

    expect(app.isRegistrationPage).toBeTrue();
  });

  it("should navigate to the register page", () => {
    app.goToRegistration();

    expect(router.navigate).toHaveBeenCalledOnceWith(["/register"]);
  });
});
