import { CdkDragDrop, DragDropModule } from "@angular/cdk/drag-drop";
import { HttpClientModule } from "@angular/common/http";
import { EventEmitter } from "@angular/core";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule, NgForm } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { of } from "rxjs";
import { ConfirmationDialogComponent } from "../../confirmation-dialog/confirmation-dialog.component";
import { MockAuthService } from "../../mocks/mock-auth.service";
import { MockRouter } from "../../mocks/mock-router";
import { TestData } from "../../mocks/test-data";
import { Recipe } from "../recipe/recipe";
import { RecipeFactory } from "../recipe/recipe-factory";
import { AuthService } from "../services/auth/auth.service";
import { EditRecipeService } from "../services/edit-recipe/edit-recipe.service";
import { RecipesService } from "../services/recipes/recipes.service";
import { RecipeFormComponent } from "./recipe-form.component";

describe("RecipeFormComponent", () => {
  let component: RecipeFormComponent;
  let fixture: ComponentFixture<RecipeFormComponent>;
  let testRec: Recipe;
  let mockRecipeService: jasmine.SpyObj<RecipesService>;
  let editRecipeService: EditRecipeService;
  let mockRouteParamsEmitter: EventEmitter<Params>;

  beforeEach(waitForAsync(() => {
    mockRecipeService = jasmine.createSpyObj("MockRecipeService", [
      "addRecipe",
      "deleteRecipe",
      "getRecipe",
      "getRecipes",
      "updateRecipe",
    ]);
    mockRouteParamsEmitter = new EventEmitter<Params>();

    TestBed.configureTestingModule({
      declarations: [ConfirmationDialogComponent, RecipeFormComponent],
      imports: [
        BrowserAnimationsModule,
        DragDropModule,
        FormsModule,
        HttpClientModule,
        MatIconModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { params: mockRouteParamsEmitter },
        },
        { provide: AuthService, useClass: MockAuthService },
        EditRecipeService,
        { provide: RecipeFactory, useValue: { buildRecipes: () => null } },
        { provide: RecipesService, useValue: mockRecipeService },
        { provide: Router, useClass: MockRouter },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    testRec = TestData.getTestRecipe();
    editRecipeService = TestBed.inject(EditRecipeService);
    fixture = TestBed.createComponent(RecipeFormComponent);
    component = fixture.componentInstance;

    component.deleteDialog = {
      dialogClosed: new EventEmitter<boolean>(),
    } as unknown as ConfirmationDialogComponent;

    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });

  it("should have correct create config", () => {
    expect(component.editRecipe.ingredients.length).toBe(1);
    expect(component.editRecipe.directions.length).toBe(1);
    expect(component.editState).toBe(false);
  });

  it("should toggle editState", () => {
    component.toggleEditState();
    expect(component.editState).toBe(true);
    component.toggleEditState();
    expect(component.editState).toBe(false);
    component.toggleEditState();
    expect(component.editState).toBe(true);
  });

  it("should call recipeService.addRecipe", () => {
    mockRecipeService.addRecipe.and.returnValue(of());

    component.saveRecipe(new NgForm([], []));

    expect(mockRecipeService.addRecipe).toHaveBeenCalledTimes(1);
  });

  it("should call recipeService.updateRecipe", () => {
    mockRecipeService.updateRecipe.and.returnValue(of());

    component.updateRecipe(new NgForm([], []));

    expect(mockRecipeService.updateRecipe).toHaveBeenCalledTimes(1);
  });

  it("should correctly build a recipe", () => {
    component.editRecipe.name = testRec.name;
    component.editRecipe.prepDuration = testRec.prepDuration;
    component.editRecipe.cookDuration = testRec.cookDuration;
    component.editRecipe.servings = testRec.servings;
    component.editRecipe.ingredients = testRec.ingredients;
    component.editRecipe.directions = testRec.directions;
    component.editRecipe.userId = "usernamehash123";
    component.editRecipe._id = testRec._id;

    expect(component.editRecipe).toEqual(testRec);
  });

  it("should correctly unpack the recipe", () => {
    component.editRecipe = testRec;

    expect(component.editRecipe.name).toEqual(testRec.name);
    expect(component.editRecipe.prepDuration).toEqual(testRec.prepDuration);
    expect(component.editRecipe.cookDuration).toEqual(testRec.cookDuration);
    expect(component.editRecipe.servings).toEqual(testRec.servings);
    expect(component.editRecipe.ingredients).toEqual(testRec.ingredients);
    expect(component.editRecipe.directions).toEqual(testRec.directions);
    expect(component.editRecipe.userId).toEqual(testRec.userId);
    expect(component.editRecipe._id).toEqual(testRec._id);
  });

  it("should add a blank Ingredient", () => {
    expect(component.editRecipe.ingredients.length).toBe(1);

    component.addIngredient();

    expect(component.editRecipe.ingredients.length).toBe(2);
  });

  it("should add a blank Direction", () => {
    expect(component.editRecipe.directions.length).toBe(1);

    component.addDirection();

    expect(component.editRecipe.directions.length).toBe(2);
  });

  it("should remove an Ingredient", () => {
    expect(component.editRecipe.ingredients.length).toBe(1);
    component.addIngredient();
    component.addIngredient();
    component.addIngredient();
    expect(component.editRecipe.ingredients.length).toBe(4);

    component.confirmDeleteIngredient(component.editRecipe.ingredients[1]);
    component.deleteDialog.dialogClosed.emit(true);

    expect(component.editRecipe.ingredients.length).toBe(3);
  });

  it("should remove a Direction", () => {
    expect(component.editRecipe.directions.length).toBe(1);
    component.addDirection();
    component.addDirection();
    expect(component.editRecipe.directions.length).toBe(3);

    component.confirmDeleteDirection(component.editRecipe.directions[1]);
    component.deleteDialog.dialogClosed.emit(true);

    expect(component.editRecipe.directions.length).toBe(2);
  });

  it("should call correct method based on editState var", () => {
    const updateRecipeSpy = spyOn(component, "updateRecipe");
    const saveRecipeSpy = spyOn(component, "saveRecipe");

    component.onFormSubmit(new NgForm([], []));

    expect(saveRecipeSpy).toHaveBeenCalledTimes(1);
    component.toggleEditState();
    component.onFormSubmit(new NgForm([], []));
    expect(updateRecipeSpy).toHaveBeenCalledTimes(1);
  });

  it("should unpack a testRec and the edit state should be true", () => {
    expect(component.editState).toBe(false);

    editRecipeService.newEditRecipe.emit(TestData.getTestRecipe());

    expect(component.editState).toBe(true);
    expect(component.editRecipe).toEqual(TestData.getTestRecipe());
  });

  it("should remove ingredient properly", () => {
    component.editRecipe = testRec;
    component.confirmDeleteIngredient(component.editRecipe.ingredients[0]);

    component.deleteDialog.dialogClosed.emit(true);

    expect(component.editRecipe.ingredients.length).toBe(1);
  });

  it("should not remove ingredient due to no result", () => {
    component.editRecipe = testRec;
    component.confirmDeleteIngredient(component.editRecipe.ingredients[0]);

    component.deleteDialog.dialogClosed.emit(false);

    expect(component.editRecipe.ingredients.length).toBe(2);
  });

  it("should remove direction properly", () => {
    component.editRecipe = testRec;
    component.confirmDeleteDirection(component.editRecipe.directions[0]);

    component.deleteDialog.dialogClosed.emit(true);

    expect(component.editRecipe.directions.length).toBe(1);
  });

  it("should detect the route param id", () => {
    const testRecipe = TestData.getRecipesResponse();
    mockRecipeService.getRecipe.and.returnValue(of(testRecipe));

    mockRouteParamsEmitter.emit({ id: "123" });

    expect(mockRecipeService.getRecipe).toHaveBeenCalledTimes(1);
    expect(mockRecipeService.getRecipe).toHaveBeenCalledWith("123");
    expect(component.editState).toBe(true);
  });

  it("should move a Direction by drop", () => {
    component.editRecipe = testRec;

    component.onDirectionDrop({
      previousIndex: 0,
      currentIndex: 1,
    } as unknown as CdkDragDrop<string[]>);

    expect(component.editRecipe.directions[0].text).toBe("Boil the water");
  });

  it("should move an Ingredient by drop", () => {
    component.editRecipe = testRec;

    component.onIngredientDrop({
      previousIndex: 0,
      currentIndex: 1,
    } as unknown as CdkDragDrop<string[]>);

    expect(component.editRecipe.ingredients[0].name).toBe("water");
  });

  it("should delete recipe", () => {
    component.editRecipe = testRec;
    const serviceDelSpy = mockRecipeService.deleteRecipe.and.returnValue(
      of({ msg: "ok" })
    );
    const hideRecipeFormSpy = spyOn(component, "hideRecipeForm");

    component.confirmDeleteRecipe();
    component.deleteDialog.dialogClosed.emit(true);

    expect(serviceDelSpy).toHaveBeenCalledWith(testRec);
    expect(hideRecipeFormSpy).toHaveBeenCalled();
  });
});
