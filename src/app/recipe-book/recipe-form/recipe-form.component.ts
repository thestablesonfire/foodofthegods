import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationDialogComponent } from "../../confirmation-dialog/confirmation-dialog.component";
import { Direction } from "../recipe/direction/direction";
import { Ingredient } from "../recipe/ingredient/ingredient";
import { Recipe } from "../recipe/recipe";
import { RecipeFactory } from "../recipe/recipe-factory";
import { AuthService } from "../services/auth/auth.service";
import { EditRecipeService } from "../services/edit-recipe/edit-recipe.service";
import { RecipesService } from "../services/recipes/recipes.service";

@Component({
  selector: "app-recipe-form",
  styleUrls: ["./recipe-form.component.scss"],
  templateUrl: "./recipe-form.component.html",
})
export class RecipeFormComponent implements OnInit {
  @ViewChild("deleteDialog") public deleteDialog: ConfirmationDialogComponent;
  @ViewChild("recipeForm") public recipeForm: NgForm;

  public dialogItemName: string;
  public editState: boolean;
  public editRecipe: Recipe;
  public showDialog: boolean;

  constructor(
    private authService: AuthService,
    private editRecipeService: EditRecipeService,
    private recipeService: RecipesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.editRecipe = RecipeFormComponent.getNewRecipe(
      this.authService.getUsername()
    );
    this.editState = false;
  }

  private static getNewDirection(): Direction {
    return new Direction(null, null);
  }

  private static getNewIngredient(): Ingredient {
    return new Ingredient(null, null, null);
  }

  private static getNewRecipe(username: string): Recipe {
    return new Recipe(
      null,
      null,
      null,
      null,
      [RecipeFormComponent.getNewIngredient()],
      [RecipeFormComponent.getNewDirection()],
      username
    );
  }

  private static removeFromArray(
    array: Direction[] | Ingredient[],
    index: number
  ): void {
    array.splice(index, 1);
  }

  public addDirection(): void {
    this.editRecipe.directions.push(RecipeFormComponent.getNewDirection());
  }

  public addIngredient(): void {
    this.editRecipe.ingredients.push(RecipeFormComponent.getNewIngredient());
  }

  public confirmDeleteDirection(direction: Direction): void {
    const index = this.editRecipe.directions.indexOf(direction);

    this.confirmDeleteItem(direction.text, () => {
      RecipeFormComponent.removeFromArray(this.editRecipe.directions, index);
    });
  }

  public confirmDeleteIngredient(ingredient: Ingredient): void {
    const index = this.editRecipe.ingredients.indexOf(ingredient);

    this.confirmDeleteItem(ingredient.name, () => {
      RecipeFormComponent.removeFromArray(this.editRecipe.ingredients, index);
    });
  }

  public confirmDeleteRecipe(): void {
    this.confirmDeleteItem(this.editRecipe.name, () => {
      this.deleteRecipe();
    });
  }

  public hideRecipeForm(form: NgForm): void {
    this.resetForm(form);
    this.editState = false;
    this.router.navigate(["recipes"]);
  }

  public setSubscribers(): void {
    this.editRecipeService.newEditRecipe.subscribe((recipe) => {
      this.editRecipe = recipe;
      this.editState = true;
    });
  }

  public ngOnInit(): void {
    this.setSubscribers();

    this.route.params.subscribe((params) => {
      const id = params.id;

      if (id) {
        this.editState = true;
        this.recipeService.getRecipe(id).subscribe((recipe) => {
          this.editRecipe = RecipeFactory.buildRecipes(recipe)[0];
        });
      }
    });
  }

  public onDirectionDrop(e: CdkDragDrop<string[]>): void {
    moveItemInArray(
      this.editRecipe.directions,
      e.previousIndex,
      e.currentIndex
    );
  }

  public onFormSubmit(form: NgForm): void {
    this.editState ? this.updateRecipe(form) : this.saveRecipe(form);
  }

  public onIngredientDrop(e: CdkDragDrop<string[]>): void {
    moveItemInArray(
      this.editRecipe.ingredients,
      e.previousIndex,
      e.currentIndex
    );
  }

  public saveRecipe(form: NgForm): void {
    this.recipeService.addRecipe(this.editRecipe).subscribe(() => {
      this.hideRecipeForm(form);
    });
  }

  public toggleEditState(): void {
    this.editState = !this.editState;
  }

  public updateRecipe(form: NgForm): void {
    this.recipeService.updateRecipe(this.editRecipe).subscribe(() => {
      this.hideRecipeForm(form);
    });
  }

  private confirmDeleteItem(
    displayPropertyName: string,
    resultFunction: () => void
  ): void {
    this.dialogItemName = displayPropertyName;

    const subscription = this.deleteDialog.dialogClosed.subscribe(
      (result: boolean) => {
        if (result) {
          resultFunction();
        }

        this.showDialog = false;
        subscription.unsubscribe();
      }
    );

    this.showDialog = true;
  }

  private deleteRecipe(): void {
    this.recipeService.deleteRecipe(this.editRecipe).subscribe(() => {
      this.hideRecipeForm(this.recipeForm);
    });
  }

  private resetForm(form: NgForm): void {
    form.resetForm();
    this.resetRecipeComponents();
  }

  private resetRecipeComponents(): void {
    this.editRecipe.ingredients = [RecipeFormComponent.getNewIngredient()];
    this.editRecipe.directions = [RecipeFormComponent.getNewDirection()];
  }
}
