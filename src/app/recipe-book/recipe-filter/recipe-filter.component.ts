import { Component, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-recipe-filter",
  templateUrl: "./recipe-filter.component.html",
})
export class RecipeFilterComponent {
  @Output() public filterStringUpdated = new EventEmitter<string>();
  public filterString = "";
}
