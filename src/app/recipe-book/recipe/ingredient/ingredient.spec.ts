import { Ingredient } from "./ingredient";

describe("Ingredient Class", () => {
  let ingredient: Ingredient;
  const ingName = "Salt";
  const ingAmount = 1;
  const ingUnit = "c";
  const ingId = 123;

  beforeEach(() => {
    ingredient = new Ingredient(ingName, ingUnit, ingAmount, ingId);
  });

  it("should be created", () => {
    expect(ingredient).toBeTruthy();
  });

  it("should return the correct values", () => {
    expect(ingredient.name).toEqual(ingName);
    expect(ingredient.amount).toEqual(ingAmount);
    expect(ingredient.unit).toEqual(ingUnit);
    expect(ingredient.id).toEqual(ingId);
  });

  it("should generate a new ID", () => {
    ingredient = new Ingredient(ingName, ingUnit, ingAmount);

    expect(ingredient.id).toBeTruthy();
  });
});
