import { Utils } from "src/app/utils";

export class Ingredient {
  constructor(
    public name: string,
    public unit: string,
    public amount: number,
    public id: number = Utils.getNewId()
  ) {}

  public getDescriptionString(): string {
    const amount = `${this.amount} `;
    const unit = this.unit ? `${this.unit} ` : "";
    const name = this.name;

    return `${amount}${unit}${name}`;
  }
}
