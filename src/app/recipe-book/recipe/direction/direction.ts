import { Utils } from "../../../utils";

export class Direction {
  constructor(
    public text: string,
    public duration: string = "",
    public id: number = Utils.getNewId()
  ) {}
}
