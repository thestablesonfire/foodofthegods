import { Direction } from "./direction";

describe("Direction Class", () => {
  let direction: Direction;
  const dirText = "Do one thing please";
  const dirDuration = "5 min.";
  const dirId = 123;

  beforeEach(() => {
    direction = new Direction(dirText, dirDuration, dirId);
  });

  it("should be created", () => {
    expect(direction).toBeTruthy();
  });

  it("should return the correct values", () => {
    expect(direction.text).toEqual(dirText);
    expect(direction.duration).toEqual(dirDuration);
    expect(direction.id).toEqual(dirId);
  });

  it("should return the correct values", () => {
    direction = new Direction(dirText);

    expect(direction.text).toEqual(dirText);
    expect(direction.duration).toEqual("");
  });
});
