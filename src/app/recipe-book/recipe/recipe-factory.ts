import { Injectable } from "@angular/core";
import { IngredientListDto } from "src/app/dto/ingredient-list.dto";
import { DirectionDto, IngredientDto, RecipeDto } from "src/app/dto/recipe.dto";

import { IngredientList } from "src/app/ingredient-list/ingredient-list";
import { RecipeListItem } from "../recipe-list/recipe-list-item";
import { Direction } from "./direction/direction";
import { Ingredient } from "./ingredient/ingredient";
import { Recipe } from "./recipe";

@Injectable()
export class RecipeFactory {
  private static buildDirections(directionDto: DirectionDto[]): Direction[] {
    const directionList = [];

    for (const direction of directionDto) {
      directionList.push(
        new Direction(direction.text, direction.duration, direction.id)
      );
    }

    return directionList;
  }

  private static buildIngredients(
    ingredientDto: IngredientDto[]
  ): Ingredient[] {
    const ingredientList = [];

    for (const ingredient of ingredientDto) {
      ingredientList.push(
        new Ingredient(
          ingredient.name.toLowerCase(),
          ingredient.unit,
          ingredient.amount,
          ingredient.id
        )
      );
    }

    return ingredientList;
  }

  public static buildIngredientList(
    ingredientListDto: IngredientListDto
  ): IngredientList {
    const ingredientList = [];
    let ingredient: Ingredient;

    ingredientListDto.items = ingredientListDto.items || [];

    for (const ingredientListItem of ingredientListDto.items) {
      ingredient = new Ingredient(
        ingredientListItem.ingredient.name,
        ingredientListItem.ingredient.unit,
        ingredientListItem.ingredient.amount,
        ingredientListItem.ingredient.id
      );
      ingredientList.push({
        ingredient,
        completed: ingredientListItem.completed,
      });
    }

    return new IngredientList(
      ingredientList,
      new Date(ingredientListDto.lastModified)
    );
  }

  public static buildRecipeListItems(
    recipeListItemsDto: RecipeDto[]
  ): RecipeListItem[] {
    const descriptions: RecipeListItem[] = [];

    for (const description of recipeListItemsDto) {
      descriptions.push({
        name: description.name,
        prepDuration: description.prepDuration,
        cookDuration: description.cookDuration,
        _id: description._id,
      });
    }

    return descriptions;
  }

  public static buildRecipes(recipesDto: RecipeDto[]): Recipe[] {
    const recipeList = [];

    for (const recipe of recipesDto) {
      const name = recipe.name;
      const prepDuration = recipe.prepDuration;
      const cookDuration = recipe.cookDuration;
      const servings = parseFloat(recipe.servings);
      const ingredients = RecipeFactory.buildIngredients(recipe.ingredients);
      const directions = RecipeFactory.buildDirections(recipe.directions);
      const userId = recipe.userId;
      const id = recipe._id;

      recipeList.push(
        new Recipe(
          name,
          prepDuration,
          cookDuration,
          servings,
          ingredients,
          directions,
          userId,
          id
        )
      );
    }

    return recipeList;
  }
}
