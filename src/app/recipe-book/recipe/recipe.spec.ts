import { TestData } from "../../mocks/test-data";
import { Direction } from "./direction/direction";
import { Ingredient } from "./ingredient/ingredient";
import { Recipe } from "./recipe";

describe("Recipe Class", () => {
  let recipe: Recipe;

  beforeEach(() => {
    recipe = TestData.getTestRecipe();
  });

  it("should be created", () => {
    expect(recipe).toBeTruthy();
  });

  it("should get all data correctly", () => {
    recipe._id = "123";
    expect(recipe.name).toEqual("salt water");
    expect(recipe.prepDuration).toEqual("10 min.");
    expect(recipe.cookDuration).toEqual("10 min.");
    expect(recipe.servings).toEqual(4);
    expect(recipe.ingredients).toEqual([
      new Ingredient("salt", "T", 1, 123),
      new Ingredient("water", "c", 2, 234),
    ]);
    expect(recipe.directions).toEqual([
      new Direction("Put the salt in the water", "5 min.", 1),
      new Direction("Boil the water", "5 min.", 2),
    ]);
    expect(recipe._id).toEqual("123");
    expect(recipe.userId).toEqual("usernamehash123");
  });

  it("should set all data correctly", () => {
    recipe._id = "123";
    recipe.name = "NEW REC";
    recipe.userId = "123hash";

    expect(recipe._id).toEqual("123");
    expect(recipe.name).toEqual("NEW REC");
    expect(recipe.userId).toEqual("123hash");
  });

  it("should modify ingredient amounts by servings", () => {
    const ingredients = recipe.getDisplayIngredients(4);
    const ingredientsDouble = recipe.getDisplayIngredients(8);
    const ingredientsHalf = recipe.getDisplayIngredients(2);
    const ingredientsThreeQuarters = recipe.getDisplayIngredients(3);

    expect(ingredients[0].amount).toEqual(1);
    expect(ingredients[1].amount).toEqual(2);
    expect(ingredientsDouble[0].amount).toEqual(2);
    expect(ingredientsDouble[1].amount).toEqual(4);
    expect(ingredientsHalf[0].amount).toEqual(0.5);
    expect(ingredientsHalf[1].amount).toEqual(1);
    expect(ingredientsThreeQuarters[0].amount).toEqual(0.75);
    expect(ingredientsThreeQuarters[1].amount).toEqual(1.5);
  });
});
