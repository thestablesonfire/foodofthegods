import { Direction } from "./direction/direction";
import { Ingredient } from "./ingredient/ingredient";

export class Recipe {
  constructor(
    public name: string,
    public prepDuration: string,
    public cookDuration: string,
    public servings: number,
    public ingredients: Ingredient[],
    public directions: Direction[],
    public userId: string,
    public _id?: string
  ) {}

  public getDisplayIngredients(servings: number): Ingredient[] {
    let ingredients: Ingredient[];

    if (!servings || servings === this.servings) {
      ingredients = this.ingredients;
    } else {
      const originalServings = this.servings;
      const newIngredients = this.copyIngredients();

      for (const ingredient of newIngredients) {
        const baseServings = ingredient.amount / originalServings;

        ingredient.amount = baseServings * servings;
      }

      ingredients = newIngredients;
    }

    return ingredients;
  }

  private copyIngredients(): Ingredient[] {
    const ingredientsCopy = [];

    for (const ingredient of this.ingredients) {
      ingredientsCopy.push(
        new Ingredient(ingredient.name, ingredient.unit, ingredient.amount)
      );
    }

    return ingredientsCopy;
  }
}
