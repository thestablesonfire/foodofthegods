import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";

import { MockAuthService } from "../../../mocks/mock-auth.service";
import { MockRouter } from "../../../mocks/mock-router";
import { AuthGuardService } from "./auth-guard.service";
import { AuthService } from "./auth.service";

describe("AuthGuardService", () => {
  let authService: MockAuthService;
  let router: MockRouter;
  let service: AuthGuardService;

  beforeEach(() => {
    authService = new MockAuthService();
    router = new MockRouter();

    TestBed.configureTestingModule({
      providers: [
        AuthGuardService,
        { provide: AuthService, useValue: authService },
        { provide: Router, useValue: router },
      ],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(AuthGuardService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should not be able to activate the route", () => {
    const authSpy = authService.isAuthenticated.and.returnValue(false);
    const routerSpy = router.navigate;

    expect(service.canActivate()).toEqual(false);
    expect(authSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["/login"]);
  });

  it("should be able to activate the route", () => {
    const authSpy = authService.isAuthenticated.and.returnValue(true);

    expect(service.canActivate()).toEqual(true);
    expect(authSpy).toHaveBeenCalledTimes(1);
  });
});
