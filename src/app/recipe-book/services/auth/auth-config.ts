export class AuthConfig {
  public headers: { "X-Access-Token": string };

  constructor(token?: string) {
    this.headers = {
      "X-Access-Token": token,
    };
  }

  public getHeaders(): { "X-Access-Token": string } {
    return this.headers;
  }
}
