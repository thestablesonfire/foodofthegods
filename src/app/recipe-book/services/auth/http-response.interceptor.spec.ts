import { TestBed } from "@angular/core/testing";
import { HttpResponseInterceptor } from "./http-response.interceptor";
import { MockAuthService } from "src/app/mocks/mock-auth.service";
import { AuthService } from "./auth.service";

describe("HttpResponseInterceptor", () => {
  let service: HttpResponseInterceptor;
  let authService: MockAuthService;

  beforeEach(() => {
    authService = new MockAuthService();

    TestBed.configureTestingModule({
      providers: [{ provide: AuthService, useValue: authService }],
    });
    service = TestBed.inject(HttpResponseInterceptor);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
