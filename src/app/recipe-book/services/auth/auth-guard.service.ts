import { inject, Injectable } from "@angular/core";
import { CanActivateFn, Router } from "@angular/router";

import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuardService {
  constructor(private auth: AuthService, private router: Router) {}

  public canActivate(): boolean {
    this.auth.activityHasOccurred = true;

    if (this.auth.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(["/login"]);

      return false;
    }
  }
}

export const isAuthenticated: CanActivateFn = () => {
  return inject(AuthGuardService).canActivate();
};
