import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { Router } from "@angular/router";
import md5 from "md5";
import { CookieService } from "ngx-cookie-service";
import pbkdf2 from "pbkdf2";
import { Observable, map, switchMap } from "rxjs";
import { environment } from "../../../../environments/environment";
import {
  PasswordResponseDto,
  UsernameResponseDto,
} from "../../../dto/user.dto";
import { AuthConfig } from "../auth/auth-config";

@Injectable()
export class AuthService {
  private readonly cookieExpirationTime: number;
  private readonly sessionExpirationTime: number;
  private password: string;
  private username: string;

  public activityHasOccurred: boolean;
  public activityTimer: NodeJS.Timeout;
  public baseURL: string;
  public cookieName: string;
  public token: string;
  public tokenUpdatedEvent: EventEmitter<null>;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router
  ) {
    this.baseURL = environment.baseUrl + "/token";
    this.cookieName = "FOTG_AUTH_TOKEN";
    this.tokenUpdatedEvent = new EventEmitter();
    this.activityHasOccurred = false;
    this.sessionExpirationTime = 3600000; // 1 hr in ms
    this.cookieExpirationTime =
      (this.sessionExpirationTime - 60000) / (1000 * 60 * 60 * 24); // 59 min as a fraction of a day
    this.username = AuthService.getUsernameFromLocalStorage();
  }

  private static getUsernameFromLocalStorage(): string {
    return localStorage.getItem("username");
  }

  private static unsetLocalStorage(): void {
    localStorage.removeItem("username");
  }

  public static hashPassword(date: string, password: string): string {
    const salt = md5(date);
    const passHash = md5(password);

    return (
      salt + pbkdf2.pbkdf2Sync(passHash, salt, 10, 32, "sha512").toString("hex")
    );
  }

  public getConfig(): AuthConfig {
    return new AuthConfig(this.token);
  }

  public getCookie(): string {
    return this.cookieService.get(this.cookieName);
  }

  public getToken(username: string, password: string): Observable<string> {
    this.username = username;
    this.password = password;

    return this.http
      .get<UsernameResponseDto>(this.baseURL + "/" + username)
      .pipe(
        switchMap((usernameResponse: UsernameResponseDto) => {
          return this.http.post(this.baseURL, {
            password: AuthService.hashPassword(
              usernameResponse.timestamp,
              password
            ),
            username: usernameResponse.username,
          });
        }),
        map((passwordResponse: PasswordResponseDto) => {
          this.setToken(passwordResponse.token);

          return this.token;
        })
      );
  }

  public getUsername(): string {
    return this.username;
  }

  public isAuthenticated(): boolean {
    const token = this.getCookie();

    // console.log("isAuth", token);

    if (token) {
      this.setToken(token);
      this.setLocalStorage();
    } else {
      this.deleteCookie();
      AuthService.unsetLocalStorage();
    }

    return this.cookieService.check(this.cookieName);
  }

  public logOut(): void {
    this.deleteCookie();
    AuthService.unsetLocalStorage();
    this.activityHasOccurred = false;
    clearInterval(this.activityTimer);
    this.router.navigate(["/login"]);
  }

  public setToken(token: string): void {
    this.token = token;
    this.setCookie(token);
    this.tokenUpdatedEvent.emit();
  }

  private deleteCookie(): void {
    this.cookieService.delete(this.cookieName);
    this.token = "";
    this.tokenUpdatedEvent.emit();
  }

  // TODO: Write a test for this
  private setActivityTimer(): void {
    this.activityHasOccurred = false;

    this.activityTimer = setInterval(() => {
      if (this.activityHasOccurred) {
        this.activityHasOccurred = false;
        this.getToken(this.username, this.password).subscribe({
          next: (token) => {
            if (token) {
              this.setToken(token);
            } else {
              this.logOut();
            }
          },
          error: () => {
            console.log("error?");
            this.logOut();
          },
        });
      } else {
        this.logOut();
      }
    }, this.sessionExpirationTime);
  }

  private setCookie(cValue: string): void {
    this.cookieService.set(this.cookieName, cValue, {
      expires: this.cookieExpirationTime,
    });

    if (!this.activityTimer) {
      this.setActivityTimer();
    }
  }

  private setLocalStorage(): void {
    if (this.username) {
      localStorage.setItem("username", this.username);
    }
  }
}
