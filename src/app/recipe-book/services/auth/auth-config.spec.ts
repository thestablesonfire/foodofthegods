import { AuthConfig } from "./auth-config";

describe("AuthConfig", () => {
  let config: AuthConfig;

  beforeEach(() => {
    config = new AuthConfig("123");
  });

  it("should create an instance", () => {
    expect(config).toBeTruthy();
  });

  it("should have the right token", () => {
    expect(config.getHeaders()).toEqual({ "X-Access-Token": "123" });
  });
});
