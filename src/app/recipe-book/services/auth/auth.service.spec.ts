import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import md5 from "md5";
import { CookieService } from "ngx-cookie-service";
import { firstValueFrom, of } from "rxjs";
import { MockCookieService } from "../../../mocks/mock-cookie.service";
import { MockRouter } from "../../../mocks/mock-router";
import { AuthService } from "./auth.service";

describe("AuthService", () => {
  let service: AuthService;
  let http: HttpClient;
  let cookieService: MockCookieService;

  beforeEach(() => {
    cookieService = new MockCookieService();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        { provide: CookieService, useValue: cookieService },
        { provide: Router, useClass: MockRouter },
      ],
    });
  });

  beforeEach(() => {
    service = TestBed.inject(AuthService);
    http = TestBed.inject(HttpClient);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should authenticate successfully", async () => {
    const hashedUsername = md5("123");
    const getSpy = spyOn(http, "get").and.returnValue(
      of({
        success: true,
        timestamp: "Mon Jan 08 2018 10:52:50 GMT-0500 (EST)",
        username: "202cb962ac59075b964b07152d234b70",
      })
    );
    const postSpy = spyOn(http, "post").and.returnValue(
      of({
        message: "authenticated",
        success: true,
        token: "123",
      })
    );

    await firstValueFrom(
      service.getToken(
        "202cb962ac59075b964b07152d234b70",
        "81dc9bdb52d04dc20036dbd8313ed055"
      )
    );

    expect(getSpy).toHaveBeenCalledTimes(1);
    expect(getSpy).toHaveBeenCalledWith(service.baseURL + "/" + hashedUsername);

    expect(postSpy).toHaveBeenCalledTimes(1);
    expect(postSpy).toHaveBeenCalledWith(service.baseURL, {
      password:
        "0dbb474670844e04d9e7a45d95d82af17cb90d45c1a63700ed19b35eb39a85626e0d5b6307737fe140057f822069d9ff",
      username: hashedUsername,
    });
  });

  it("should return the correct config", () => {
    service.token = "123";

    const config = service.getConfig();

    expect(config.headers["X-Access-Token"]).toEqual("123");
  });

  it("should set the new token", () => {
    const tokenUpdatedSpy = spyOn(service.tokenUpdatedEvent, "emit");

    service.setToken("123");

    expect(service.token).toEqual("123");
    expect(cookieService.set).toHaveBeenCalledTimes(1);
    expect(tokenUpdatedSpy).toHaveBeenCalledTimes(1);
  });

  it("should set a cookie correctly", () => {
    const setCookieSpy = cookieService.set;
    service.cookieName = "test";

    service.setToken("123");

    expect(setCookieSpy).toHaveBeenCalledTimes(1);
    expect(setCookieSpy).toHaveBeenCalledWith("test", "123", {
      expires: 0.04097222222222222,
    });
  });

  it("should get a cookie correctly", () => {
    cookieService.get.and.returnValue("123");
    service.cookieName = "test";
    service.setToken("123");

    const token = service.getCookie();

    expect(token).toEqual("123");
  });

  it("should log the user out", () => {
    cookieService.check.and.returnValue(false);
    const tokenUpdateSpy = spyOn(service.tokenUpdatedEvent, "emit");
    const localStorageSpy = spyOn(localStorage, "removeItem");

    service.logOut();

    expect(cookieService.delete).toHaveBeenCalledTimes(1);
    expect(tokenUpdateSpy).toHaveBeenCalledTimes(1);
    expect(localStorageSpy).toHaveBeenCalledTimes(1);
    expect(service.token).toEqual("");
    expect(service.isAuthenticated()).toBe(false);
  });

  it("should verify that the user is authenticated", () => {
    cookieService.get.and.returnValue("123");
    const setTokenSpy = spyOn(service, "setToken").and.callThrough();
    cookieService.check.and.returnValue(true);
    service.cookieName = "test";

    service.setToken("123");

    expect(service.isAuthenticated()).toBe(true);
    expect(setTokenSpy).toHaveBeenCalledTimes(2);
    expect(cookieService.get).toHaveBeenCalledTimes(1);
    expect(cookieService.check).toHaveBeenCalledTimes(1);
  });

  it("should verify that the user is NOT authenticated", () => {
    cookieService.get.and.returnValue("");
    cookieService.check.and.returnValue(false);
    service.cookieName = "test";

    expect(service.isAuthenticated()).toBe(false);
    expect(cookieService.get).toHaveBeenCalledTimes(1);
    expect(cookieService.check).toHaveBeenCalledTimes(1);
    expect(service.token).toBe("");
  });

  it("should return username", () => {
    service["username"] = "123hash";

    expect(service.getUsername()).toEqual("123hash");
  });

  it("should set local storage", () => {
    const localSpy = spyOn(localStorage, "setItem");
    const cookieSpy = spyOn(service, "getCookie").and.returnValue("123");
    service["username"] = "testUser";

    service.isAuthenticated();

    expect(localSpy).toHaveBeenCalledWith("username", "testUser");
    expect(cookieSpy).toHaveBeenCalledTimes(1);

    service["username"] = "";

    service.isAuthenticated();
    expect(cookieSpy).toHaveBeenCalledTimes(2);
    expect(localSpy).toHaveBeenCalledTimes(1);
  });
});
