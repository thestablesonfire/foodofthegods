/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { map, Observable } from "rxjs";
import { AuthService } from "./auth.service";
import { RequestResponseDto } from "src/app/dto/response.dto";

@Injectable({
  providedIn: "root",
})
export class HttpResponseInterceptor implements HttpInterceptor {
  public constructor(private authService: AuthService) {}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((e: HttpEvent<any>) => {
        if (e instanceof HttpResponse) {
          return this.validateResponse(e);
        }
      })
    );
  }

  private validateResponse<T>(
    req: HttpResponse<RequestResponseDto<T>>
  ): HttpResponse<T> {
    const reqBody = req.body;

    if (reqBody.data && reqBody.success) {
      return req.clone({ body: reqBody.data });
    } else {
      this.authService.logOut();
    }
  }
}
