import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {
  AddRecipeDto,
  DeleteRecipeDto,
  RecipeDto,
  UpdateRecipeDto,
} from "src/app/dto/recipe.dto";
import { environment } from "../../../../environments/environment";
import { RecipeListItem } from "../../recipe-list/recipe-list-item";
import { Recipe } from "../../recipe/recipe";
import { AuthService } from "../auth/auth.service";

@Injectable()
export class RecipesService {
  public baseURL: string;
  public recipeBaseUrl: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.baseURL = environment.baseUrl + "/recipes/";
    this.recipeBaseUrl = environment.baseUrl + "/recipe";
  }

  public addRecipe(recipe: Recipe): Observable<AddRecipeDto> {
    return this.http.post<AddRecipeDto>(
      this.baseURL,
      recipe,
      this.authService.getConfig()
    );
  }

  public deleteRecipe(
    recipe: Recipe | RecipeListItem
  ): Observable<DeleteRecipeDto> {
    return this.http.delete<DeleteRecipeDto>(
      this.baseURL + recipe._id,
      this.authService.getConfig()
    );
  }

  public getRecipe(id: string): Observable<RecipeDto[]> {
    return this.http.get<RecipeDto[]>(this.recipeBaseUrl + "/" + id);
  }

  public getRecipes(): Observable<RecipeDto[]> {
    return this.http.get<RecipeDto[]>(
      this.baseURL + this.authService.getUsername(),
      this.authService.getConfig()
    );
  }

  public updateRecipe(recipe: Recipe): Observable<UpdateRecipeDto> {
    return this.http.put<UpdateRecipeDto>(
      this.baseURL + recipe._id,
      recipe,
      this.authService.getConfig()
    );
  }
}
