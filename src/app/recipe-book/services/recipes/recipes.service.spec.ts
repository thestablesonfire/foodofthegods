import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { CookieService } from "ngx-cookie-service";
import { firstValueFrom, of } from "rxjs";
import { MockAuthService } from "../../../mocks/mock-auth.service";
import { MockCookieService } from "../../../mocks/mock-cookie.service";
import { TestData } from "../../../mocks/test-data";
import { AuthConfig } from "../auth/auth-config";
import { AuthService } from "../auth/auth.service";
import { RecipesService } from "./recipes.service";

describe("RecipesService", () => {
  let service: RecipesService;
  let authService: MockAuthService;
  let http: HttpClient;

  beforeEach(() => {
    authService = new MockAuthService();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: CookieService, useClass: MockCookieService },
        RecipesService,
      ],
    });
  });

  beforeEach(() => {
    service = TestBed.inject(RecipesService);
    http = TestBed.inject(HttpClient);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should call get and return values", () => {
    const usernameSpy = authService.getUsername.and.returnValue("test123");
    authService.token = "123";
    const httpSpy = spyOn(http, "get").and.returnValue(
      of(TestData.getRecipesResponse())
    );

    service.getRecipes();

    expect(httpSpy).toHaveBeenCalledTimes(1);
    expect(httpSpy).toHaveBeenCalledWith(
      service.baseURL + "test123",
      new AuthConfig("123")
    );
    expect(usernameSpy).toHaveBeenCalledTimes(1);
  });

  it("should call post and return success", () => {
    const httpSpy = spyOn(http, "post").and.returnValue(
      of({ success: true, data: true })
    );
    authService.token = "123";

    service.addRecipe(TestData.getTestRecipe());

    expect(httpSpy).toHaveBeenCalledTimes(1);
    expect(httpSpy).toHaveBeenCalledWith(
      service.baseURL,
      TestData.getTestRecipe(),
      new AuthConfig("123")
    );
  });

  it("should call delete and return success", () => {
    const testRec = TestData.getTestRecipe();
    const httpSpy = spyOn(http, "delete").and.returnValue(
      of({ success: true, data: true })
    );
    testRec._id = "1234";

    service.deleteRecipe(testRec);

    expect(httpSpy).toHaveBeenCalledTimes(1);
    expect(httpSpy).toHaveBeenCalledWith(
      service.baseURL + "1234",
      new AuthConfig("123")
    );
  });

  it("should call put and return success", () => {
    const testRec = TestData.getTestRecipe();
    const httpSpy = spyOn(http, "put").and.returnValue(
      of({ success: true, data: true })
    );
    testRec.name = "New Name";
    testRec._id = "1234";

    service.updateRecipe(testRec);

    expect(httpSpy).toHaveBeenCalledTimes(1);
    expect(httpSpy).toHaveBeenCalledWith(
      service.baseURL + "1234",
      testRec,
      new AuthConfig("123")
    );
  });

  it("should get single recipe", async () => {
    spyOn(http, "get").and.returnValue(of(TestData.getRecipesResponse()));

    const data = await firstValueFrom(service.getRecipe("607"));

    expect(data[0].name).toEqual("Spicy Garlic-Lime Chicken");
  });

  it("should return empty array with no data property returned", async () => {
    spyOn(http, "get").and.returnValue(of([]));

    const data = await firstValueFrom(service.getRecipes());

    expect(data).toEqual([]);
  });
});
