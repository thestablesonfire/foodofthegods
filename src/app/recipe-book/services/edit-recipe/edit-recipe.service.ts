import { EventEmitter, Injectable } from "@angular/core";

import { Recipe } from "../../recipe/recipe";

@Injectable()
export class EditRecipeService {
  public newEditRecipe: EventEmitter<Recipe>;

  constructor() {
    this.newEditRecipe = new EventEmitter();
  }
}
