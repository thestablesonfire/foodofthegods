import { HttpClientModule } from "@angular/common/http";
import {
  ComponentFixture,
  TestBed,
  TestModuleMetadata,
  waitForAsync,
} from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Observable, of } from "rxjs";
import { IngredientListService } from "../../ingredient-list/ingredient-list.service";
import { MockAuthService } from "../../mocks/mock-auth.service";
import { MockCookieService } from "../../mocks/mock-cookie.service";
import { MockIngredientListService } from "../../mocks/mock-ingredient-list.service";
import { MockRecipeFactory } from "../../mocks/mock-recipe.factory";
import { MockRouter } from "../../mocks/mock-router";
import { TestData } from "../../mocks/test-data";
import { RecipeFactory } from "../recipe/recipe-factory";
import { AuthService } from "../services/auth/auth.service";
import { EditRecipeService } from "../services/edit-recipe/edit-recipe.service";
import { RecipesService } from "../services/recipes/recipes.service";
import { RecipeViewerComponent } from "./recipe-viewer.component";
import { RecipeDto } from "src/app/dto/recipe.dto";
import { MatIconModule } from "@angular/material/icon";

describe("RecipeViewerComponent", () => {
  let component: RecipeViewerComponent;
  let fixture: ComponentFixture<RecipeViewerComponent>;
  let activatedRoute: ActivatedRoute;
  let router: MockRouter;
  let ingredientListService: MockIngredientListService;
  let authService: MockAuthService;
  let mockRouteParams: Observable<Params>;

  describe("happy-path route params", () => {
    beforeEach(async () => {
      authService = new MockAuthService();
      ingredientListService = new MockIngredientListService();
      router = new MockRouter();
      mockRouteParams = of({ id: "123" });
      TestBed.configureTestingModule(getTestConfig()).compileComponents();
      activatedRoute = TestBed.inject(ActivatedRoute);
      fixture = TestBed.createComponent(RecipeViewerComponent);
      component = fixture.componentInstance;
      component.activeRecipe = TestData.getTestRecipe();
      fixture.detectChanges();
    });

    it("should be created", () => {
      expect(component).toBeTruthy();
      expect(component.activeRecipe).toEqual(TestData.getTestRecipe());
    });

    it("should have a share id", () => {
      activatedRoute.params = of({ shareId: "123" });
      const completeSpy = spyOn(component, "buildDirectionsComplete");

      component.ngOnInit();

      expect(component.activeRecipe).toEqual(TestData.getTestRecipe());
      expect(completeSpy).toHaveBeenCalledTimes(1);
    });

    it("should hide recipe viewer", () => {
      const routerSpy = router.navigate;

      component.hideRecipeViewer();

      expect(routerSpy).toHaveBeenCalledOnceWith(["recipes"]);
    });

    it("should navigate to the recipe form for editing", () => {
      const routerSpy = router.navigate;
      component.activeRecipe._id = "123";

      component.editRecipe();

      expect(routerSpy).toHaveBeenCalledOnceWith(["recipes/edit", "123"]);
    });

    it("should add the ingredient to the list correctly", () => {
      const expectedIng = TestData.getTestRecipe().ingredients[0];
      const ingredientListSpy = ingredientListService.addIngredient;

      component.addIngredientToList(0);

      expect(ingredientListSpy).toHaveBeenCalledTimes(1);
      expect(ingredientListSpy).toHaveBeenCalledWith(expectedIng);
    });

    it("should add all of the ingredients to the list", () => {
      const expectedIngs = TestData.getTestRecipe().ingredients;
      const ingredientListSpy = ingredientListService.addIngredients;

      component.addAllIngredientsToList();

      expect(ingredientListSpy).toHaveBeenCalledOnceWith(expectedIngs);
    });

    it("should add the multiplied servings ingredient to the list", () => {
      component.displayedServings = component.displayedServings * 2;
      const expectedIngs = TestData.getTestRecipe().getDisplayIngredients(
        component.displayedServings
      );
      const ingredientListSpy = ingredientListService.addIngredients;
      2;
      component.addAllIngredientsToList();

      expect(ingredientListSpy).toHaveBeenCalledOnceWith(expectedIngs);
    });

    it("should toggle the directions completed", () => {
      expect(component.directionsComplete).toEqual([false, false]);
      component.toggleDirectionCompleted(0);
      expect(component.directionsComplete).toEqual([true, false]);
      component.toggleDirectionCompleted(1);
      expect(component.directionsComplete).toEqual([true, true]);
      component.toggleDirectionCompleted(0);
      expect(component.directionsComplete).toEqual([false, true]);
    });

    it("should get the correct share URL", () => {
      component.activeRecipe._id = "TEST";

      const shareURL = component.getShareURL();

      expect(shareURL).toEqual(
        "https://theunderempire.com/foodofthegods/recipes/share/TEST"
      );
    });

    it("recipe should be owned by user", () => {
      authService.getUsername.and.returnValue("usernamehash123");

      expect(component.recipeIsOwnedByUser()).toBe(true);
    });

    it("recipe should not be owned by user", () => {
      authService.getUsername.and.returnValue("test123");

      expect(component.recipeIsOwnedByUser()).toBe(false);
    });

    it("should update the displayed list of ingredients, good value", () => {
      expect(component.displayedServings).toEqual(4);

      component.displayedServings = 8;
      component.onDisplayedServingsChange();

      expect(component.activeIngredients[0].amount).toEqual(2);
      expect(component.activeIngredients[1].amount).toEqual(4);
    });

    it("should update the displayed list of ingredients, invalid value", () => {
      component.displayedServings = -1;

      component.onDisplayedServingsChange();

      expect(component.displayedServings).toEqual(4);
    });
  });

  describe("no route id", () => {
    beforeEach(waitForAsync(() => {
      authService = new MockAuthService();
      ingredientListService = new MockIngredientListService();
      router = new MockRouter();
      mockRouteParams = of({});
      TestBed.configureTestingModule(getTestConfig()).compileComponents();
    }));

    beforeEach(async () => {
      activatedRoute = TestBed.inject(ActivatedRoute);
      fixture = TestBed.createComponent(RecipeViewerComponent);
      component = fixture.componentInstance;
      component.activeRecipe = TestData.getTestRecipe();
      fixture.detectChanges();
    });

    it("should not have an id, therefore nothing should happen", () => {
      const completeSpy = spyOn(component, "buildDirectionsComplete");
      const consoleSpy = spyOn(console, "error");

      component.ngOnInit();

      expect(completeSpy).toHaveBeenCalledTimes(0);
      expect(consoleSpy).toHaveBeenCalledOnceWith(
        "No recipe id to fetch recipe"
      );
    });
  });

  function getTestConfig(): TestModuleMetadata {
    return {
      declarations: [RecipeViewerComponent],
      imports: [FormsModule, HttpClientModule, MatIconModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: mockRouteParams,
          },
        },
        { provide: AuthService, useValue: authService },
        { provide: CookieService, useClass: MockCookieService },
        EditRecipeService,
        { provide: IngredientListService, useValue: ingredientListService },
        {
          provide: RecipesService,
          useValue: {
            getRecipe(): Observable<RecipeDto[]> {
              return of([JSON.parse(JSON.stringify(TestData.getTestRecipe()))]);
            },
          },
        },
        { provide: Router, useValue: router },
        { provide: RecipeFactory, useClass: MockRecipeFactory },
      ],
    };
  }
});
