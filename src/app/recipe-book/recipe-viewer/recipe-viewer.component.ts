import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { IngredientListService } from "../../ingredient-list/ingredient-list.service";
import { Ingredient } from "../recipe/ingredient/ingredient";
import { Recipe } from "../recipe/recipe";
import { RecipeFactory } from "../recipe/recipe-factory";
import { AuthService } from "../services/auth/auth.service";
import { RecipesService } from "../services/recipes/recipes.service";
import { RecipeDto } from "src/app/dto/recipe.dto";

@Component({
  selector: "app-recipe-viewer",
  styleUrls: ["./recipe-viewer.component.scss"],
  templateUrl: "./recipe-viewer.component.html",
})
export class RecipeViewerComponent implements OnInit {
  public activeIngredients: Ingredient[];
  public activeRecipe: Recipe;
  public directionsComplete: boolean[];
  public displayedServings: number;
  public isShareLink: boolean;

  constructor(
    private authService: AuthService,
    private ingredientListService: IngredientListService,
    private recipesService: RecipesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.directionsComplete = [];
    this.isShareLink = false;
  }

  private displayRecipe(data: RecipeDto[], isShareLink: boolean): void {
    this.isShareLink = isShareLink;
    this.activeRecipe = RecipeFactory.buildRecipes(data)[0];
    this.activeIngredients = this.activeRecipe.getDisplayIngredients(
      this.displayedServings
    );
    this.displayedServings = this.activeRecipe.servings;
    this.buildDirectionsComplete();
  }

  public addAllIngredientsToList(): void {
    this.ingredientListService.addIngredients(
      this.activeRecipe.getDisplayIngredients(this.displayedServings)
    );
  }

  public addIngredientToList(index: number): void {
    const ingredientToAdd = this.activeRecipe
      .getDisplayIngredients(this.displayedServings)
      .slice()[index];
    this.ingredientListService.addIngredient(ingredientToAdd);
  }

  public buildDirectionsComplete(): void {
    const directionLength = this.activeRecipe.directions.length;

    this.directionsComplete = [];

    for (let i = 0; i < directionLength; i++) {
      this.directionsComplete.push(false);
    }
  }

  public editRecipe(): void {
    this.router.navigate(["recipes/edit", this.activeRecipe._id]);
  }

  public getShareURL(): string {
    return (
      "https://theunderempire.com/foodofthegods/recipes/share/" +
      this.activeRecipe._id
    );
  }

  public hideRecipeViewer(): void {
    this.router.navigate(["recipes"]);
  }

  public ngOnInit(): void {
    this.route.params.subscribe((params) => {
      let recipeId: string;
      let isShareLink: boolean;

      if (params.shareId) {
        recipeId = params.shareId;
        isShareLink = true;
      } else if (params.id) {
        recipeId = params.id;
        isShareLink = false;
      }

      if (recipeId) {
        this.recipesService.getRecipe(recipeId).subscribe((data) => {
          this.displayRecipe(data, isShareLink);
        });
      } else {
        console.error("No recipe id to fetch recipe");
      }
    });
  }

  public onDisplayedServingsChange(): void {
    if (this.displayedServings <= 0) {
      this.displayedServings = this.activeRecipe.servings;
    }

    this.activeIngredients = [];
    this.activeIngredients = this.activeRecipe.getDisplayIngredients(
      this.displayedServings
    );
  }

  public recipeIsOwnedByUser(): boolean {
    return this.authService.getUsername() === this.activeRecipe.userId;
  }

  public toggleDirectionCompleted(index: number): void {
    this.directionsComplete[index] = !this.directionsComplete[index];
  }
}
