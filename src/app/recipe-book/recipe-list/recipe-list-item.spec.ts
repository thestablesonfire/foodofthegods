import { RecipeListItem } from "./recipe-list-item";

describe("Recipe List Item Class", () => {
  let testItem: RecipeListItem;

  beforeEach(() => {
    testItem = {
      name: "test name",
      prepDuration: "prep duration",
      cookDuration: "cook duration",
      _id: "123",
    };
  });

  it("should be created", () => {
    expect(testItem).toBeTruthy();
  });

  it("should get the values", () => {
    expect(testItem.name).toEqual("test name");
    expect(testItem.prepDuration).toEqual("prep duration");
    expect(testItem.cookDuration).toEqual("cook duration");
    expect(testItem._id).toEqual("123");
  });
});
