import { Component, EventEmitter, Input, Output } from "@angular/core";
import { RecipeListItem } from "../recipe-list-item";

@Component({
  selector: "app-recipe-list-item",
  templateUrl: "./recipe-list-item.component.html",
  styleUrls: ["./recipe-list-item.component.scss"],
})
export class RecipeListItemComponent {
  @Output() public recipeClicked = new EventEmitter<string>();
  @Input() public recipe: RecipeListItem;
}
