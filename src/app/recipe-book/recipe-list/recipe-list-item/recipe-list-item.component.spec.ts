import { ComponentFixture, TestBed } from "@angular/core/testing";

import { RecipeListItemComponent } from "./recipe-list-item.component";

describe("RecipeListItemComponent", () => {
  let component: RecipeListItemComponent;
  let fixture: ComponentFixture<RecipeListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecipeListItemComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeListItemComponent);
    component = fixture.componentInstance;

    component.recipe = {
      name: "test rec",
      prepDuration: "1 min",
      cookDuration: "2 min.",
      _id: "idstring123",
    };

    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
