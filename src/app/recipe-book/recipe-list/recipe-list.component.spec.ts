import { HttpClientModule } from "@angular/common/http";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { ConfirmationDialogComponent } from "../../confirmation-dialog/confirmation-dialog.component";
import { MockAuthService } from "../../mocks/mock-auth.service";
import { MockRouter } from "../../mocks/mock-router";
import { TestData } from "../../mocks/test-data";
import { RecipeFilterComponent } from "../recipe-filter/recipe-filter.component";
import { AuthService } from "../services/auth/auth.service";
import { EditRecipeService } from "../services/edit-recipe/edit-recipe.service";
import { RecipesService } from "../services/recipes/recipes.service";
import { RecipeListItem } from "./recipe-list-item";
import { RecipeListComponent } from "./recipe-list.component";
import { RecipeListItemComponent } from "./recipe-list-item/recipe-list-item.component";

describe("RecipeListComponent", () => {
  let component: RecipeListComponent;
  let fixture: ComponentFixture<RecipeListComponent>;
  let mockRecipeService: jasmine.SpyObj<RecipesService>;
  let router: MockRouter;

  beforeEach(waitForAsync(() => {
    mockRecipeService = jasmine.createSpyObj("MockRecipeService", [
      "addRecipe",
      "deleteRecipe",
      "getRecipe",
      "getRecipes",
      "updateRecipe",
    ]);

    mockRecipeService.getRecipes.and.returnValue(
      of(TestData.getRecipesResponse())
    );

    router = new MockRouter();

    TestBed.configureTestingModule({
      declarations: [
        ConfirmationDialogComponent,
        RecipeListComponent,
        RecipeFilterComponent,
        RecipeListItemComponent,
      ],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatIconModule,
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        EditRecipeService,
        { provide: RecipesService, useValue: mockRecipeService },
        { provide: Router, useValue: router },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });

  it("should call the recipesService", () => {
    const getRecipesSpy = spyOn(component, "getRecipeList");

    component.ngOnInit();

    expect(getRecipesSpy).toHaveBeenCalledTimes(1);
  });

  it("should have the correct number of recipes", () => {
    expect(component.allRecipes.length).toEqual(2);
  });

  it("should remove a recipe", () => {
    mockRecipeService.deleteRecipe.and.returnValue(of({ msg: "ok" }));

    component.deleteRecipe(1);

    expect(component.allRecipes.length).toEqual(1);
    expect(mockRecipeService.deleteRecipe).toHaveBeenCalledTimes(1);
  });

  it("should toggle the edit state", () => {
    expect(component.editState).toBe(false);
    component.toggleEditState();
    expect(component.editState).toBe(true);
    component.toggleEditState();
    expect(component.editState).toBe(false);
  });

  it("should call the tabControlService to show the recipe viewer", () => {
    const routerSpy = router.navigate;
    const id = "123";

    component.showRecipeInView(id);

    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["./recipes/recipe", id]);
  });

  it("should call the tabControlService to show the recipe form for new", () => {
    const routerSpy = router.navigate;

    component.showRecipeForm();

    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["/recipes/add"]);
  });

  it("should call the tabControlService to show the recipe form to edit", () => {
    const routerSpy = router.navigate;
    const toggleSpy = spyOn(component, "toggleEditState");
    const id = "123";

    component.editRecipe(1, id);

    expect(routerSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy).toHaveBeenCalledWith(["/recipes/edit", id]);
    expect(toggleSpy).toHaveBeenCalledTimes(1);
  });

  it("should call the correct function depending on editState", () => {
    expect(component.editState).toBe(false);
    const viewRecipeSpy = spyOn(component, "showRecipeInView");

    component.onRecipeClick(0, "59f39803cf794009d3783f63");

    expect(viewRecipeSpy).toHaveBeenCalledTimes(1);
  });

  it("should filter for search by recipe name", () => {
    const searchPhrase = "choColAte";

    component.updateFilterResults(searchPhrase);

    expect(component.displayedRecipes.length).toBe(1);
    expect(component.displayedRecipes[0]).toBe(component.allRecipes[0]);
  });

  it("should sort the recipe list", () => {
    const expectedRecipeListItems: RecipeListItem[] = [
      {
        name: "Chocolate Morning Muffins",
        prepDuration: "20 min.",
        cookDuration: "20 min",
        _id: "59f77c6cd865453d9a12b8c0",
      },
      {
        name: "Spicy Garlic-Lime Chicken",
        prepDuration: "5 min.",
        cookDuration: "20 min.",
        _id: "59f39803cf794009d3783f63",
      },
    ];

    expect(component.allRecipes).toEqual(expectedRecipeListItems);
  });
});
