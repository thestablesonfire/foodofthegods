export interface RecipeListItem {
  name: string;
  prepDuration: string;
  cookDuration: string;
  _id: string;
}
