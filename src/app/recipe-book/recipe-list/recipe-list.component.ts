import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { RecipeFactory } from "../recipe/recipe-factory";
import { RecipesService } from "../services/recipes/recipes.service";
import { RecipeListItem } from "./recipe-list-item";

@Component({
  selector: "app-recipe-list",
  styleUrls: ["./recipe-list.component.scss"],
  templateUrl: "./recipe-list.component.html",
})
export class RecipeListComponent implements OnInit {
  public allRecipes: RecipeListItem[];
  public displayedRecipes: RecipeListItem[];
  public editState: boolean;
  public loadingRecipes: boolean;

  constructor(private recipeService: RecipesService, private router: Router) {
    this.editState = false;
    this.displayedRecipes = [];
  }

  public deleteRecipe(index: number): void {
    const recipe = this.allRecipes[index];

    this.recipeService.deleteRecipe(recipe).subscribe(() => {
      this.removeRecipeFromArray(index);
    });
  }

  public editRecipe(index: number, id: string): void {
    this.router.navigate(["/recipes/edit", id]);
    this.toggleEditState();
  }

  public getRecipeList(): void {
    this.loadingRecipes = true;

    this.recipeService.getRecipes().subscribe((res) => {
      this.allRecipes = RecipeFactory.buildRecipeListItems(res);
      this.sortRecipesList();
      this.clearSearchPhrase();
      this.loadingRecipes = false;
    });
  }

  public ngOnInit(): void {
    this.getRecipeList();
  }

  public onRecipeClick(index: number, id: string): void {
    this.showRecipeInView(id);
  }

  public showRecipeForm(): void {
    this.router.navigate(["/recipes/add"]);
  }

  public showRecipeInView(id: string): void {
    this.router.navigate(["./recipes/recipe", id]);
  }

  public toggleEditState(): void {
    this.editState = !this.editState;
  }

  public updateFilterResults(filterString: string): void {
    let recipesToDisplay = this.allRecipes;

    filterString.split(", ").forEach((rawPhrase) => {
      recipesToDisplay = recipesToDisplay.filter((recipe) => {
        return recipe.name
          .toLocaleLowerCase()
          .includes(rawPhrase.toLowerCase());
      });
    });

    this.displayedRecipes = recipesToDisplay;
  }

  private clearSearchPhrase(): void {
    this.displayedRecipes = this.allRecipes.slice();
  }

  private removeRecipeFromArray(index: number): void {
    this.allRecipes.splice(index, 1);
    this.clearSearchPhrase();
  }

  private sortRecipesList(): void {
    if (this.allRecipes) {
      this.allRecipes.sort((a: RecipeListItem, b: RecipeListItem) => {
        const aName = a.name;
        const bName = b.name;

        if (aName > bName) {
          return 1;
        }

        if (aName < bName) {
          return -1;
        }

        return 0;
      });
    }
  }
}
