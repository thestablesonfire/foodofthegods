import { Ingredient } from "./recipe-book/recipe/ingredient/ingredient";
import { TrueCountPipe } from "./true-count.pipe";

describe("TrueCountPipe", () => {
  let pipe: TrueCountPipe;

  beforeEach(() => {
    pipe = new TrueCountPipe();
  });

  it("create an instance", () => {
    expect(pipe).toBeTruthy();
  });

  it("filter a list of IngredientListItems", () => {
    const ingredient = new Ingredient("name", "c", 1);
    const boolTest1 = [{ ingredient, completed: false }];
    const boolTest2 = [{ ingredient, completed: true }];
    const boolTest3 = [
      { ingredient, completed: true },
      { ingredient, completed: false },
      { ingredient, completed: false },
      { ingredient, completed: true },
      { ingredient, completed: false },
      { ingredient, completed: true },
      { ingredient, completed: false },
    ];

    expect(pipe.transform(boolTest1)).toEqual(1);
    expect(pipe.transform(boolTest2)).toEqual(0);
    expect(pipe.transform(boolTest3)).toEqual(4);
  });
});
