import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { NgForm } from "@angular/forms";

import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { IngredientList } from "./ingredient-list";
import { IngredientListItem } from "./ingredient-list-item/ingredient-list-item";
import { IngredientListService } from "./ingredient-list.service";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-ingredient-list",
  styleUrls: ["./ingredient-list.component.scss"],
  templateUrl: "./ingredient-list.component.html",
})
export class IngredientListComponent implements OnInit {
  @ViewChild("ingredientName", { static: true })
  public ingredientNameField: ElementRef<HTMLInputElement>;
  public ingredientList: IngredientList;
  public newIngredientName: string;
  public newIngredientUnit: string;
  public newIngredientAmount: number;
  public isEditingIngredient: boolean;
  public editedIngredientIndex: number;

  constructor(private ingredientListService: IngredientListService) {
    this.setSubscribers();
    this.resetNewIngredient();
    this.ingredientList = new IngredientList([]);
    this.isEditingIngredient = false;
    this.editedIngredientIndex = null;
  }

  public clearIngredientList(): void {
    if (!this.isEditingIngredient) {
      this.ingredientListService.clearIngredients();
      this.updateIngredientList();
    }
  }

  public editIngredient(ingredientListItem: IngredientListItem): void {
    this.isEditingIngredient = true;
    this.editedIngredientIndex = this.ingredientList
      .getItems()
      .indexOf(ingredientListItem);
    this.newIngredientName = ingredientListItem.ingredient.name;
    this.newIngredientUnit = ingredientListItem.ingredient.unit;
    this.newIngredientAmount = ingredientListItem.ingredient.amount;
  }

  public getIngredientList(): void {
    this.ingredientListService.getIngredientList();
  }

  public ngOnInit(): void {
    this.getIngredientList();
  }

  public onIngredientFormCancel(form: NgForm): void {
    this.isEditingIngredient = false;
    this.resetForm(form);
  }

  public onIngredientFormSubmit(form: NgForm): void {
    if (this.isEditingIngredient) {
      this.updateExistingIngredient();
    } else {
      this.addNewIngredient();
    }

    this.resetForm(form);
    this.ingredientNameField.nativeElement.focus();
  }

  public removeIngredient(ingredientListItem: IngredientListItem): void {
    if (!this.isEditingIngredient) {
      this.ingredientListService.removeIngredient(
        this.ingredientList.getItems().indexOf(ingredientListItem)
      );
    }
  }

  public toggleIngredient(item: IngredientListItem): void {
    if (!this.isEditingIngredient) {
      item.completed = !item.completed;
      this.updateIngredientList();
    }
  }

  private addNewIngredient(): void {
    this.ingredientListService.addIngredient(
      new Ingredient(
        this.newIngredientName,
        this.newIngredientUnit,
        this.newIngredientAmount
      )
    );
  }

  private updateExistingIngredient(): void {
    const itemToEdit =
      this.ingredientList.getItems()[this.editedIngredientIndex];

    itemToEdit.ingredient = new Ingredient(
      this.newIngredientName,
      this.newIngredientUnit,
      this.newIngredientAmount,
      itemToEdit.ingredient.id
    );
    this.isEditingIngredient = false;
    this.editedIngredientIndex = null;
    this.updateIngredientList();
  }

  private resetNewIngredient(): void {
    this.newIngredientName = "";
    this.newIngredientUnit = "";
    this.newIngredientAmount = null;
  }

  private resetForm(form: NgForm): void {
    form.resetForm();
    this.resetNewIngredient();
  }

  private setSubscribers(): void {
    this.ingredientListService.ingredientsChangedEvent.subscribe(() => {
      this.ingredientList = this.ingredientListService.ingredientList;
    });
  }

  private updateIngredientList(): void {
    this.ingredientListService.saveIngredientList();
  }
}
