import { IngredientListItem } from "./ingredient-list-item/ingredient-list-item";

export class IngredientList {
  private items: IngredientListItem[];
  private lastModified: Date;

  constructor(
    items: IngredientListItem[] = [],
    lastModified: Date = new Date()
  ) {
    this.items = items;
    this.lastModified = lastModified;
  }

  public addItem(item: IngredientListItem): void {
    this.items.push(item);
    this.updateLastModified();
  }

  public addItems(items: IngredientListItem[]): void {
    this.items = this.items.concat(items);
    this.updateLastModified();
  }

  public getItems(): IngredientListItem[] {
    return this.items.slice();
  }

  public getLastModified(): Date {
    return new Date(this.lastModified);
  }

  public setItems(items: IngredientListItem[]): void {
    const modified = !!this.items.length;

    this.items = items;

    if (modified) {
      this.updateLastModified();
    }
  }

  private updateLastModified(): void {
    this.lastModified = new Date();
  }
}
