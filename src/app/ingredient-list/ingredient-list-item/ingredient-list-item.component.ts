import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IngredientListItem } from "./ingredient-list-item";

@Component({
  selector: "app-ingredient-list-item",
  templateUrl: "./ingredient-list-item.component.html",
  styleUrls: ["./ingredient-list-item.component.scss"],
})
export class IngredientListItemComponent {
  @Input() public index: number;
  @Input() public ingredientListItem: IngredientListItem;
  @Input() public isEditingIngredient: boolean;
  @Output() public ingredientToEdit: EventEmitter<IngredientListItem>;
  @Output() public ingredientToRemove: EventEmitter<IngredientListItem>;
  @Output() public ingredientToToggle: EventEmitter<IngredientListItem>;

  public constructor() {
    this.ingredientToEdit = new EventEmitter<IngredientListItem>();
    this.ingredientToRemove = new EventEmitter<IngredientListItem>();
    this.ingredientToToggle = new EventEmitter<IngredientListItem>();
  }

  public editIngredient(): void {
    this.ingredientToEdit.emit(this.ingredientListItem);
  }

  public removeIngredient(): void {
    this.ingredientToRemove.emit(this.ingredientListItem);
  }

  public toggleIngredient(): void {
    this.ingredientToToggle.emit(this.ingredientListItem);
  }
}
