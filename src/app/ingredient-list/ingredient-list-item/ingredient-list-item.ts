import { Ingredient } from "../../recipe-book/recipe/ingredient/ingredient";

export interface IngredientListItem {
  ingredient: Ingredient;
  completed: boolean;
}
