import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MatIconModule } from "@angular/material/icon";

import { Ingredient } from "../../recipe-book/recipe/ingredient/ingredient";
import { IngredientListItemComponent } from "./ingredient-list-item.component";

describe("IngredientListItemComponent", () => {
  let component: IngredientListItemComponent;
  let fixture: ComponentFixture<IngredientListItemComponent>;
  let editSpy: jasmine.Spy;
  let removeSpy: jasmine.Spy;
  let toggleSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IngredientListItemComponent],
      imports: [MatIconModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientListItemComponent);
    component = fixture.componentInstance;

    component.ingredientListItem = {
      ingredient: new Ingredient("test ing", "t", 1, 12345),
      completed: false,
    };
    editSpy = spyOn(component.ingredientToEdit, "emit");
    removeSpy = spyOn(component.ingredientToRemove, "emit");
    toggleSpy = spyOn(component.ingredientToToggle, "emit");

    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should emit edit Ingredient", () => {
    component.editIngredient();

    expect(editSpy).toHaveBeenCalledOnceWith(component.ingredientListItem);
  });

  it("should emit remove Ingredient", () => {
    component.removeIngredient();

    expect(removeSpy).toHaveBeenCalledOnceWith(component.ingredientListItem);
  });

  it("should emit toggle Ingredient", () => {
    component.toggleIngredient();

    expect(toggleSpy).toHaveBeenCalledOnceWith(component.ingredientListItem);
  });
});
