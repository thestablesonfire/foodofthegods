import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { CookieService } from "ngx-cookie-service";
import { of } from "rxjs";
import { IngredientListContainerDto } from "../dto/ingredient-list.dto";
import { MockAuthService } from "../mocks/mock-auth.service";
import { MockCookieService } from "../mocks/mock-cookie.service";
import { TestData } from "../mocks/test-data";
import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { IngredientList } from "./ingredient-list";
import { IngredientListItem } from "./ingredient-list-item/ingredient-list-item";
import { IngredientListService } from "./ingredient-list.service";

describe("IngredientListService", () => {
  let authService: MockAuthService;
  let ingredientsChangedSpy: jasmine.Spy;
  let ingredientListResponse: IngredientListContainerDto[];
  let service: IngredientListService;
  let http: HttpClient;
  let getSpy: jasmine.Spy;
  let postSpy: jasmine.Spy;
  const baseUrl = "https://theunderempire.com:3000/";
  const ingredientListEndpoint = "ingredientList/";
  const username = "username";

  beforeEach(() => {
    authService = new MockAuthService();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: CookieService, useClass: MockCookieService },
        IngredientListService,
      ],
    });
  });

  beforeEach(() => {
    ingredientListResponse = TestData.getIngredientListResponse();
    http = TestBed.inject(HttpClient);
    getSpy = spyOn(http, "get").and.returnValue(of(ingredientListResponse));
    postSpy = spyOn(http, "post").and.returnValue(
      of({ success: true, data: { msg: "ok" } })
    );
    service = TestBed.inject(IngredientListService);
    ingredientsChangedSpy = spyOn(
      service.ingredientsChangedEvent,
      "emit"
    ).and.callThrough();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
    expect(getSpy).toHaveBeenCalledOnceWith(
      `${baseUrl}${ingredientListEndpoint}${username}`,
      authService.getConfig()
    );
    expect(service.ingredientList.getItems().length).toBe(3);
  });

  it("should add an ingredient to an existing list", () => {
    const ingToAdd = new Ingredient("addIngName", "addIngUnit", 2);

    service.addIngredient(ingToAdd);

    expect(postSpy).toHaveBeenCalledOnceWith(
      `${baseUrl}${ingredientListEndpoint}${username}`,
      {
        ingredientList: service.ingredientList,
        userId: username,
      },
      authService.getConfig()
    );

    const listItems = service.ingredientList.getItems();
    expect(listItems.length).toBe(4);
    expect(listItems[0].ingredient.name).toBe("Salt");
    expect(listItems[1].ingredient.name).toBe("Black Pepper");
    expect(listItems[2].ingredient.name).toBe("Cayenne Pepper");
    expect(listItems[3].ingredient.name).toBe(ingToAdd.name);
  });

  // it("should add an ingredient to a list when the current list doesn't exist", () => {
  //   const ingToAdd = new Ingredient("addIngName", "addIngUnit", 2);
  //   const newData = TestData.getIngredientListResponse() as any;
  //   newData.data[0] = { _id: "123", userId: "1234" };
  //   getSpy.and.returnValue(of(newData));

  //   service.addIngredient(ingToAdd);

  //   expect(service.ingredientList.getItems().length).toBe(0);
  // });

  it("should add multiple ingredients to an existing list", () => {
    const ingsToAdd = [
      new Ingredient("addIngName1", "addIngUnit1", 1),
      new Ingredient("addIngName2", "addIngUnit2", 2),
      new Ingredient("addIngName3", "addIngUnit3", 3),
      new Ingredient("addIngName4", "addIngUnit4", 4),
    ];

    service.addIngredients(ingsToAdd);

    expect(postSpy).toHaveBeenCalledOnceWith(
      `${baseUrl}${ingredientListEndpoint}${username}`,
      {
        ingredientList: service.ingredientList,
        userId: username,
      },
      authService.getConfig()
    );

    const listItems = service.ingredientList.getItems();
    expect(listItems.length).toBe(7);
    expect(listItems[0].ingredient.name).toBe("Salt");
    expect(listItems[1].ingredient.name).toBe("Black Pepper");
    expect(listItems[2].ingredient.name).toBe("Cayenne Pepper");
    expect(listItems[3].ingredient.name).toBe("addIngName1");
    expect(listItems[4].ingredient.name).toBe("addIngName2");
    expect(listItems[5].ingredient.name).toBe("addIngName3");
    expect(listItems[6].ingredient.name).toBe("addIngName4");
  });

  it("should clear the ingredient list", () => {
    service.clearIngredients();

    expect(postSpy).toHaveBeenCalledOnceWith(
      `${baseUrl}${ingredientListEndpoint}${username}`,
      {
        ingredientList: service.ingredientList,
        userId: username,
      },
      authService.getConfig()
    );
    expect(service.ingredientList.getItems().length).toBe(0);
  });

  it("should get ingredient list", () => {
    ingredientsChangedSpy.calls.reset();
    getSpy.calls.reset();
    getSpy.and.returnValue(of(ingredientListResponse));

    service.getIngredientList();

    const listItems = service.ingredientList.getItems();

    expect(ingredientsChangedSpy).toHaveBeenCalledTimes(1);
    expect(getSpy).toHaveBeenCalledTimes(1);

    expect(listItems.length).toBe(3);
    expect(listItems[0].ingredient.name).toBe("Salt");
    expect(listItems[0].ingredient.amount).toBe(0.75);
    expect(listItems[0].ingredient.unit).toBe("tsp.");
    expect(listItems[1].ingredient.name).toBe("Black Pepper");
    expect(listItems[1].ingredient.amount).toBe(0.25);
    expect(listItems[1].ingredient.unit).toBe("tsp.");
    expect(listItems[2].ingredient.name).toBe("Cayenne Pepper");
    expect(listItems[2].ingredient.amount).toBe(0.25);
    expect(listItems[2].ingredient.unit).toBe("tsp.");
  });

  it("should remove a single ingredient", () => {
    service.removeIngredient(1);

    const listItems = service.ingredientList.getItems();
    expect(listItems.length).toBe(2);
    expect(listItems[0].ingredient.name).toBe("Salt");
    expect(listItems[1].ingredient.name).toBe("Cayenne Pepper");
  });

  it("should sort the ingredient list by completed", () => {
    service.ingredientList.setItems([
      { ingredient: new Ingredient("test1", "1", 123, 123), completed: false },
      { ingredient: new Ingredient("test2", "2", 124, 124), completed: true },
      { ingredient: new Ingredient("test3", "3", 125, 125), completed: false },
    ]);

    service.saveIngredientList();

    expect(service.ingredientList.getItems()).toEqual([
      { ingredient: new Ingredient("test1", "1", 123, 123), completed: false },
      { ingredient: new Ingredient("test3", "3", 125, 125), completed: false },
      { ingredient: new Ingredient("test2", "2", 124, 124), completed: true },
    ]);
    expect(postSpy).toHaveBeenCalled();
  });

  it("should merge two lists together", () => {
    mergeLists(
      new IngredientList(
        [
          { ingredient: new Ingredient("test1", "1", 1, 1), completed: false },
          { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
          { ingredient: new Ingredient("test3", "3", 3, 3), completed: false },
        ],
        new Date("2020-09-28T19:06:45.397Z")
      ),
      new IngredientList(
        [
          { ingredient: new Ingredient("test4", "4", 4, 4), completed: false },
          { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
          { ingredient: new Ingredient("test5", "5", 5, 5), completed: false },
        ],
        new Date("2020-09-28T18:25:37.487Z")
      ),
      [
        { ingredient: new Ingredient("test1", "1", 1, 1), completed: false },
        { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
        { ingredient: new Ingredient("test3", "3", 3, 3), completed: false },
        { ingredient: new Ingredient("test4", "4", 4, 4), completed: false },
        { ingredient: new Ingredient("test5", "5", 5, 5), completed: false },
      ]
    );
  });

  it("should merge two lists together, reverse", () => {
    mergeLists(
      new IngredientList(
        [
          { ingredient: new Ingredient("test4", "4", 4, 4), completed: false },
          { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
          { ingredient: new Ingredient("test5", "5", 5, 5), completed: false },
        ],
        new Date("2020-09-28T18:25:37.487Z")
      ),
      new IngredientList(
        [
          { ingredient: new Ingredient("test1", "1", 1, 1), completed: false },
          { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
          { ingredient: new Ingredient("test3", "3", 3, 3), completed: false },
        ],
        new Date("2020-09-28T19:06:45.397Z")
      ),
      [
        { ingredient: new Ingredient("test1", "1", 1, 1), completed: false },
        { ingredient: new Ingredient("test2", "2", 2, 2), completed: false },
        { ingredient: new Ingredient("test3", "3", 3, 3), completed: false },
        { ingredient: new Ingredient("test4", "4", 4, 4), completed: false },
        { ingredient: new Ingredient("test5", "5", 5, 5), completed: false },
      ]
    );
  });

  function mergeLists(
    listA: IngredientList,
    listB: IngredientList,
    expectation: IngredientListItem[]
  ) {
    getSpy.and.returnValue(
      of([{ ingredientList: JSON.parse(JSON.stringify(listB)) }])
    );

    service.ingredientsChangedEvent.subscribe(() => {
      expect(JSON.stringify(service.ingredientList.getItems())).toEqual(
        JSON.stringify(expectation)
      );
      expect(getSpy).toHaveBeenCalled();
    });

    service.ingredientList = listA;
    service.addIngredients([]);
  }
});
