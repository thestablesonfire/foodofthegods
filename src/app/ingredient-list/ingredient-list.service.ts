import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { environment } from "../../environments/environment";
import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { RecipeFactory } from "../recipe-book/recipe/recipe-factory";
import { AuthService } from "../recipe-book/services/auth/auth.service";
import { IngredientList } from "./ingredient-list";
import { IngredientListItem } from "./ingredient-list-item/ingredient-list-item";
import {
  IngredientListContainerDto,
  IngredientListDto,
  SaveIngredientListResponse,
} from "../dto/ingredient-list.dto";

@Injectable()
export class IngredientListService {
  private baseURL = environment.baseUrl + "/ingredientList/";

  public ingredientList = new IngredientList();
  public ingredientsChangedEvent: EventEmitter<null>;

  public constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.ingredientsChangedEvent = new EventEmitter();

    if (this.authService.isAuthenticated()) {
      this.getIngredientList();
    }
  }

  private static ingredientListItemsFromIngredients(
    ingredients: Ingredient[]
  ): IngredientListItem[] {
    const ingredientListItems = [];

    for (const ingredient of ingredients) {
      ingredientListItems.push({ ingredient, completed: false });
    }

    return ingredientListItems;
  }

  public addIngredient(ingredient: Ingredient): void {
    this.fetchAndMergeLists(() => {
      this.ingredientList.addItem({ ingredient, completed: false });
    });
  }

  public addIngredients(ingredients: Ingredient[]): void {
    this.fetchAndMergeLists(() => {
      this.ingredientList.addItems(
        IngredientListService.ingredientListItemsFromIngredients(ingredients)
      );
    });
  }

  public clearIngredients(): void {
    this.ingredientList = new IngredientList([]);
    this.ingredientsChangedEvent.emit();
    this.saveIngredientList();
  }

  public getIngredientList(): void {
    this.handleFetchResponse(this.setActiveList);
  }

  public removeIngredient(index: number): void {
    const item = this.ingredientList.getItems()[index];
    let newIndex: number;
    let newList: IngredientListItem[];

    this.fetchAndMergeLists(() => {
      newList = this.ingredientList.getItems();
      newIndex = newList.indexOf(item);
      newList.splice(newIndex, 1);
      this.ingredientList.setItems(newList);
    });
  }

  public saveIngredientList(): void {
    const userId = this.authService.getUsername();
    this.sortIngredientList();

    this.http
      .post<SaveIngredientListResponse>(
        this.baseURL + userId,
        {
          ingredientList: this.ingredientList,
          userId,
        },
        this.authService.getConfig()
      )
      .subscribe(() => {
        this.ingredientsChanged();
      });
  }

  private fetchIngredientList(): Observable<IngredientListContainerDto[]> {
    return this.http.get<IngredientListContainerDto[]>(
      this.baseURL + this.authService.getUsername(),
      this.authService.getConfig()
    );
  }

  private fetchAndMergeLists(action: () => void): void {
    this.handleFetchResponse((ingredientList) => {
      this.setMergedList(ingredientList);
      action.call(this);
      this.saveIngredientList();
    });
  }

  private handleFetchResponse(action: (dto?: IngredientListDto) => void): void {
    this.fetchIngredientList().subscribe((resDto) => {
      const ingredientList = resDto[0]?.ingredientList;

      if (ingredientList) {
        action.call(this, ingredientList);
        // TODO: this should be handled by API
      } else {
        this.clearIngredients();
      }
    });
  }

  private ingredientsChanged(): void {
    this.ingredientList.setItems(this.ingredientList.getItems());
    this.ingredientsChangedEvent.emit();
  }

  private mergeIngredientLists(
    listA: IngredientList,
    listB: IngredientList
  ): IngredientListItem[] {
    let newestList: IngredientList;
    let oldestList: IngredientList;

    if (listA.getLastModified() > listB.getLastModified()) {
      newestList = listA;
      oldestList = listB;
    } else {
      newestList = listB;
      oldestList = listA;
    }

    const oldListUnique = oldestList
      .getItems()
      .filter((oldItem: IngredientListItem) => {
        return !newestList.getItems().some((newItem) => {
          return newItem.ingredient.id === oldItem.ingredient.id;
        });
      });

    return newestList.getItems().concat(oldListUnique);
  }

  private setActiveList(list: IngredientListDto): void {
    this.setIngredientList(list);
    this.ingredientsChanged();
  }

  private setIngredientList(ingredientJSON): void {
    this.ingredientList = RecipeFactory.buildIngredientList(ingredientJSON);
  }

  private sortIngredientList(): void {
    this.ingredientList.setItems(
      this.ingredientList.getItems().sort((itemA, itemB) => {
        return (itemA.completed ? 1 : 0) - (itemB.completed ? 1 : 0);
      })
    );
  }

  private setMergedList(list: IngredientListDto): void {
    const mergedList = this.mergeIngredientLists(
      this.ingredientList,
      RecipeFactory.buildIngredientList(list)
    );

    this.ingredientList.setItems(mergedList);
    this.sortIngredientList();
  }
}
