import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { IngredientList } from "./ingredient-list";

describe("IngredientList", () => {
  let lastModified: Date;
  let list: IngredientList;

  describe("Default List", () => {
    beforeEach(() => {
      list = new IngredientList();
    });

    it("should create valid list", () => {
      expect(list).toBeTruthy();
      expect(list.getItems().length).toBe(0);
      expect(list.getLastModified().constructor.name).toBeTruthy();
    });
  });

  describe("Passed IngredientListItems", () => {
    beforeEach(() => {
      lastModified = new Date("1/1/2001");
      list = new IngredientList(
        [
          { ingredient: new Ingredient("salt", "tsp", 1), completed: true },
          { ingredient: new Ingredient("pepper", "tsp", 1), completed: false },
          {
            ingredient: new Ingredient("turmeric", "tsp", 1),
            completed: false,
          },
          {
            ingredient: new Ingredient("red pepper flakes", "tsp", 2),
            completed: false,
          },
        ],
        lastModified
      );
    });

    it("should create an instance", () => {
      expect(list).toBeTruthy();
    });

    it("should add an item to the list", () => {
      const newItem = {
        ingredient: new Ingredient("newItem", "c", 1),
        completed: false,
      };
      list.addItem(newItem);

      const items = list.getItems();
      expect(items.length).toBe(5);
      expect(list.getLastModified() > lastModified).toBeTrue();
    });

    it("should add multiple items to the list", () => {
      const newItems = [
        { ingredient: new Ingredient("newItem1", "c", 1), completed: false },
        { ingredient: new Ingredient("newItem2", "c", 1), completed: false },
      ];

      list.addItems(newItems);

      const items = list.getItems();
      expect(items.length).toBe(6);
      expect(list.getLastModified() > lastModified).toBeTrue();
    });

    it("should replace items with passed list", () => {
      const newItems = [
        { ingredient: new Ingredient("newItem1", "c", 1), completed: false },
        { ingredient: new Ingredient("newItem2", "c", 1), completed: false },
      ];

      list.setItems(newItems);

      const items = list.getItems();
      expect(items.length).toBe(2);
      expect(list.getLastModified() > lastModified).toBeTrue();
    });
  });
});
