import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule, NgForm } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MockIngredientListService } from "../mocks/mock-ingredient-list.service";
import { Ingredient } from "../recipe-book/recipe/ingredient/ingredient";
import { TrueCountPipe } from "../true-count.pipe";
import { IngredientList } from "./ingredient-list";
import { IngredientListComponent } from "./ingredient-list.component";
import { IngredientListService } from "./ingredient-list.service";

describe("IngredientListComponent", () => {
  let component: IngredientListComponent;
  let fixture: ComponentFixture<IngredientListComponent>;
  let ingredientListService: MockIngredientListService;

  beforeEach(waitForAsync(() => {
    ingredientListService = new MockIngredientListService();

    TestBed.configureTestingModule({
      declarations: [IngredientListComponent, TrueCountPipe],
      imports: [BrowserAnimationsModule, FormsModule, HttpClientTestingModule],
      providers: [
        { provide: IngredientListService, useValue: ingredientListService },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.ingredientList.setItems([
      { ingredient: new Ingredient("name1", "unit1", 1), completed: false },
      { ingredient: new Ingredient("name2", "unit2", 2), completed: false },
      { ingredient: new Ingredient("name3", "unit3", 3), completed: false },
      { ingredient: new Ingredient("name4", "unit4", 4), completed: false },
    ]);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
    expect(component.newIngredientAmount).toBe(null);
    expect(component.newIngredientName).toBe("");
    expect(component.newIngredientUnit).toBe("");
    expect(component.ingredientList.getItems().length).toBe(4);
    expect(component.isEditingIngredient).toBeFalse();
    expect(component.editedIngredientIndex).toBeNull();
    expect(ingredientListService.getIngredientList).toHaveBeenCalledTimes(1);
  });

  it("should tell the ingredientListService to clear and save the IngredientList", () => {
    component.clearIngredientList();

    expect(ingredientListService.clearIngredients).toHaveBeenCalledTimes(1);
    expect(ingredientListService.saveIngredientList).toHaveBeenCalledTimes(1);
  });

  it("should set the ingredient to be edited", () => {
    const editIndex = 0;
    const ingredientToEdit = component.ingredientList.getItems()[editIndex];

    component.editIngredient(ingredientToEdit);

    expect(component.isEditingIngredient).toBeTrue();
    expect(component.editedIngredientIndex).toBe(editIndex);
    expect(component.newIngredientName).toBe(ingredientToEdit.ingredient.name);
    expect(component.newIngredientUnit).toBe(ingredientToEdit.ingredient.unit);
    expect(component.newIngredientAmount).toBe(
      ingredientToEdit.ingredient.amount
    );
  });

  it("should request the IngredientList from the IngredientListService", () => {
    ingredientListService.getIngredientList.calls.reset();

    component.getIngredientList();

    expect(ingredientListService.getIngredientList).toHaveBeenCalledTimes(1);
  });

  it("should correctly reset the ingredient form on cancel", () => {
    const formToReset = new NgForm([], []);
    const resetFormSpy = spyOn(formToReset, "resetForm");

    component.onIngredientFormCancel(formToReset);

    expect(component.isEditingIngredient).toBeFalse();
    expect(resetFormSpy).toHaveBeenCalledTimes(1);
    expect(component.newIngredientAmount).toBe(null);
    expect(component.newIngredientName).toBe("");
    expect(component.newIngredientUnit).toBe("");
  });

  it("should add a new ingredient to the list if not editing on submit", () => {
    const form = new NgForm([], []);
    const newIngAmount = 123456;
    const newIngName = "newIngName";
    const newIngUnit = "newIngUnit";

    component.newIngredientAmount = newIngAmount;
    component.newIngredientName = newIngName;
    component.newIngredientUnit = newIngUnit;

    component.onIngredientFormSubmit(form);

    const addedIngredient = ingredientListService.addIngredient.calls
      .first()
      .args.at(0);
    expect(addedIngredient.amount).toBe(newIngAmount);
    expect(addedIngredient.name).toBe(newIngName);
    expect(addedIngredient.unit).toBe(newIngUnit);
  });

  it("should edit an ingredient if editing on submit", () => {
    const form = new NgForm([], []);
    const newIngAmount = 123456;
    const newIngName = "newIngName";
    const newIngUnit = "newIngUnit";
    const editIndex = 0;

    component.isEditingIngredient = true;
    component.editedIngredientIndex = editIndex;
    component.newIngredientAmount = newIngAmount;
    component.newIngredientName = newIngName;
    component.newIngredientUnit = newIngUnit;

    component.onIngredientFormSubmit(form);

    const editedIngredient =
      component.ingredientList.getItems()[editIndex].ingredient;
    expect(editedIngredient.amount).toBe(newIngAmount);
    expect(editedIngredient.name).toBe(newIngName);
    expect(editedIngredient.unit).toBe(newIngUnit);
    expect(component.isEditingIngredient).toBeFalse();
    expect(component.editedIngredientIndex).toBeNull();
    expect(ingredientListService.saveIngredientList).toHaveBeenCalledTimes(1);
  });

  it("should request the IngredientListService to remove an ingredient", () => {
    const editIndex = 0;

    component.removeIngredient(component.ingredientList.getItems()[0]);

    expect(ingredientListService.removeIngredient).toHaveBeenCalledOnceWith(
      editIndex
    );
  });

  it("should toggle an ingredient list item and save the list", () => {
    const toggleIndex = 1;
    const ingredientToToggle = component.ingredientList.getItems()[toggleIndex];
    expect(ingredientToToggle.completed).toBeFalse();

    component.toggleIngredient(ingredientToToggle);

    expect(ingredientToToggle.completed).toBeTrue();
    expect(ingredientListService.saveIngredientList).toHaveBeenCalledTimes(1);
  });

  it("should set the ingredient list to the passed list when ingredientsChangedEvent fires", () => {
    const newIngredientList = new IngredientList();
    ingredientListService.ingredientList = newIngredientList;

    ingredientListService.ingredientsChangedEvent.emit();

    expect(component.ingredientList).toBe(newIngredientList);
  });
});
