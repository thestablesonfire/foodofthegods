export const environment = {
  production: true,
  baseUrl: "https://theunderempire.com:3000",
  architect: {
    build: {
      options: {
        baseHref: "/foodofthegods/",
      },
    },
  },
};
