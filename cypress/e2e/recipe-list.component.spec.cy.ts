describe("RecipeList", () => {
  const baseUrl = "http://localhost:4200";

  beforeEach(() => {
    cy.login();
    cy.visit("/recipes");
  });

  it("should verify page elements are present", () => {
    cy.get("h1").should("contain", "Food of the Gods");
    cy.get("#log-out-button").should("contain", "Log Out");
    cy.get("#recipes-tab").should("contain", "Recipes");
    cy.get("#ingredients-tab").should("contain", "Ingredients");
    cy.get("input[name=recipe-filter]").should("exist");
    cy.get("#recipe-list-count").should("contain", "1 Recipes");
    cy.get("#add-recipe-button").should("exist");
    cy.get("app-recipe-list-item").should("contain", "Test");
    cy.get("app-recipe-list-item").should(
      "contain",
      "Prep Time: 1 min  |  Cook Time: 2 min "
    );
  });

  it("should take you to add recipe form", () => {
    cy.get("#add-recipe-button").click();

    cy.url().should("equal", `${baseUrl}/recipes/add`);
  });

  it("should view the recipe", () => {
    cy.get("app-recipe-list-item").click();

    cy.url().should(
      "equal",
      "http://localhost:4200/recipes/recipe/6244eb84c0198f0014e646b6"
    );
  });
});
