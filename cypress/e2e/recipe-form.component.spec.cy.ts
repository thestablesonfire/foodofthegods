describe("RecipeForm", () => {
  beforeEach(() => {
    cy.login();
  });

  it("should add a new recipe", () => {
    cy.visit("/recipes/add");

    const recipeName = "New Recipe 1";
    const recipePrep = "10 min";
    const recipeCook = "20 min";
    const numServings = "4";
    const ing1Amount = "1";
    const ing1Name = "beans";
    const ing1Unit = "can";
    const ing2Amount = "2";
    const ing2Name = "salt";
    const ing2Unit = "T";
    const dir1Text = "Pour the salt onto the beans";
    const dir1Dur = "1 second";
    const dir2Text = "Continue to stir";
    const dir2Dur = "20 min.";
    const addIngredientButton = cy.get("#add-ingredient-button");
    const addDirectionButton = cy.get("#add-direction-button");

    // Recipe Info
    cy.get("input[name=recipeName]").type(recipeName);
    cy.get("input[name=recipePrepDuration]").type(recipePrep);
    cy.get("input[name=recipeCookDuration]").type(recipeCook);
    cy.get("input[name=recServings]").type(numServings);

    // Ingredients
    addIngredientToInputs(0, ing1Amount, ing1Unit, ing1Name);
    addIngredientButton.click();
    addIngredientToInputs(1, ing2Amount, ing2Unit, ing2Name);
    addIngredientButton.click();
    addIngredientToInputs(2, ing2Amount, ing2Unit, ing2Name);

    // Directions
    addDirectionToInputs(0, dir1Text, dir1Dur);
    addDirectionButton.click();
    addDirectionToInputs(1, dir2Text, dir2Dur);
    addDirectionButton.click();
    addDirectionToInputs(2, dir2Text, dir2Dur);

    deleteIngredient(2, ing2Name);
    deleteDirection(2, dir2Text);

    cy.get("#submit-recipe-form-button").click();

    cy.url().should("include", "/recipes");

    const test = cy.get(
      `app-recipe-list-item[recipeName=${recipeName.replaceAll(" ", "")}]`
    );

    test.should("exist");
    test.click();

    cy.url().should("include", "/recipes/recipe/");

    cy.get("li[name=ingredient0]").should(
      "contain",
      `${ing1Amount} ${ing1Unit} ${ing1Name}`
    );
    cy.get("li[name=ingredient1]").should(
      "contain",
      `${ing2Amount} ${ing2Unit} ${ing2Name}`
    );
    cy.get("li[name=direction0]").should("contain", dir1Text);
    cy.get("li[name=direction0]").should("contain", dir1Dur);
    cy.get("li[name=direction1]").should("contain", dir2Text);
    cy.get("li[name=direction1]").should("contain", dir2Dur);
  });

  it("should edit the newly-created recipe", () => {
    cy.visit("/recipes");
    cy.get("app-recipe-list-item[recipeName=NewRecipe1]").click();

    cy.url().should("contain", "/recipes/recipe/");
    cy.get("button[name=edit-recipe-button]").click();
    cy.url().should("contain", "/recipes/edit/");

    const recipeName = cy.get("input[name=recipeName]");
    const recipeDuration = cy.get("input[name=recipePrepDuration]");
    const recipeCook = cy.get("input[name=recipeCookDuration]");
    const recipeServings = cy.get("input[name=recServings]");

    replaceInputText(recipeName, "1");
    replaceInputText(recipeDuration, "1");
    replaceInputText(recipeCook, "1");
    replaceInputText(recipeServings, "1");

    addIngredientToInputs(0, "1", "1", "1");
    addIngredientToInputs(1, "2", "2", "2");
    addDirectionToInputs(0, "1", "2");
    addDirectionToInputs(1, "3", "4");

    cy.get("#submit-recipe-form-button").click();
    cy.url().should("include", "/recipes");
    cy.get("app-recipe-list-item[recipeName=1]").click();

    cy.get("li[name=ingredient0]").should("contain", "1 1 1");
    cy.get("li[name=ingredient1]").should("contain", "2 2 2");
    cy.get("li[name=direction0]").should("contain", "1");
    cy.get("li[name=direction0]").should("contain", "2");
    cy.get("li[name=direction1]").should("contain", "3");
    cy.get("li[name=direction1]").should("contain", "4");
  });

  it("should delete the recipe", () => {
    cy.visit("/recipes");
    cy.get("app-recipe-list-item[recipeName=1]").click();
    cy.get("button[name=edit-recipe-button]").click();
    cy.get("button[name=delete-recipe-button]").click();
    cy.wait(200);
    cy.get("section.modal-card-body").should(
      "contain",
      `Do you want to delete '1'`
    );
    cy.get("button[name=confirm-dialog]").click();

    cy.get("app-recipe-list-item[recipeName=1]").should("not.exist");
  });

  function addDirectionToInputs(index: number, text: string, duration: string) {
    const textInput = cy.get(`#direction${index}`);
    const durInput = cy.get(`#directionDuration${index}`);

    replaceInputText(textInput, text);
    replaceInputText(durInput, duration);
  }

  function addIngredientToInputs(
    index: number,
    amount: string,
    unit: string,
    name: string
  ): void {
    const amountInput = cy.get(`input[name=amount${index}]`);
    const unitInput = cy.get(`input[name=unit${index}]`);
    const nameInput = cy.get(`input[name=name${index}]`);

    replaceInputText(amountInput, amount);
    replaceInputText(unitInput, unit);
    replaceInputText(nameInput, name);
  }

  function replaceInputText(
    input: Cypress.Chainable<JQuery>,
    text: string
  ): void {
    input.clear();
    input.type(text);
  }

  function deleteDirection(i: number, expectedText: string): void {
    cy.get(`mat-icon[name=deleteDirection${i}]`).click();
    cy.wait(100);
    cy.get("section.modal-card-body").should(
      "contain",
      `Do you want to delete '${expectedText}'`
    );
    cy.get("button[name=confirm-dialog]").click();
  }

  function deleteIngredient(i: number, expectedName: string): void {
    cy.get(`mat-icon[name=deleteIngredient${i}]`).click();
    cy.wait(100);
    cy.get("section.modal-card-body").should(
      "contain",
      `Do you want to delete '${expectedName}'`
    );
    cy.get("button[name=confirm-dialog]").click();
  }
});
