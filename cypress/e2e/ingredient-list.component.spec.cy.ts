describe("IngredientList", () => {
  beforeEach(() => {
    cy.login();
  });

  it("should add ingredients to the list", () => {
    cy.visit("/ingredients");

    cy.get("button[name=ingredientListFormCancel]").click();
    cy.get("button[name=clearAllIngredients]").click();

    const ingEmptyMsg = cy.get("div[name=ingredientsEmptyMsg]");
    ingEmptyMsg.should("exist");
    ingEmptyMsg.should("contain", "Your ingredient list is empty.");

    cy.visit("/recipes");

    const testRecipe = cy.get("app-recipe-list-item[recipeName=Test]");
    testRecipe.should("exist");
    testRecipe.click();

    cy.get("button[name=addIngredientToList0]").click();

    // Double the servings
    const recipeServingsInput = cy.get("input[name=recipe-servings]");
    recipeServingsInput.clear();
    recipeServingsInput.type("90");

    cy.get("button[name=addIngredientToList1]").click();

    cy.visit("/ingredients");

    cy.get(".ingredient-list .title").should("contain", "2 ingredients to get");
    cy.get("app-ingredient-list-item[name=ingredientListItem0]").should(
      "contain",
      "1 salt"
    );
    cy.get("app-ingredient-list-item[name=ingredientListItem1]").should(
      "contain",
      "2 water"
    );
  });

  it("should edit an ingredient", () => {
    cy.visit("/ingredients");

    cy.get("button[name=editIngredientListItem0").click();
    const ingredientAmountInput = cy.get("input[name=ingredientAmount]");
    ingredientAmountInput.should("have.value", 1);

    const ingredientUnitInput = cy.get("input[name=ingredientUnit]");
    ingredientUnitInput.should("have.value", "");

    const ingredientNameInput = cy.get("input[name=ingredientName]");
    ingredientNameInput.should("have.value", "salt");

    ingredientAmountInput.clear();
    ingredientAmountInput.type("2");
    ingredientUnitInput.type("can");
    ingredientNameInput.clear();
    ingredientNameInput.type("h20");

    cy.get("button[name=ingredientListFormAdd]").click();
    cy.wait(200);
    cy.get("app-ingredient-list-item[name=ingredientListItem0]").should(
      "contain",
      "2 can h20"
    );
  });

  it("should cross and un-cross items from the list", () => {
    cy.visit("/ingredients");

    // Cross off
    cy.get("app-ingredient-list-item[name=ingredientListItem0]").click();
    // should move to bottom
    const completedItem1 = cy.get(
      "app-ingredient-list-item[name=ingredientListItem1]"
    );
    completedItem1.should("contain.text", "2 can h20");
    completedItem1.find("p.completed").should("exist");
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "1 ingredients to get"
    );

    // Cross off
    cy.get("app-ingredient-list-item[name=ingredientListItem0]").click();

    const completedItem2 = cy.get(
      "app-ingredient-list-item[name=ingredientListItem0]"
    );
    completedItem2.should("contain.text", "2 water");
    completedItem2.find("p.completed").should("exist");
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "0 ingredients to get"
    );

    // Un-cross
    cy.get("app-ingredient-list-item[name=ingredientListItem1]").click();

    const completedItem3 = cy.get(
      "app-ingredient-list-item[name=ingredientListItem0]"
    );
    completedItem3.should("contain.text", "2 can h20");
    completedItem3.find("p.completed").should("not.exist");
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "1 ingredients to get"
    );

    // Un-cross
    cy.get("app-ingredient-list-item[name=ingredientListItem1]").click();

    const completedItem4 = cy.get(
      "app-ingredient-list-item[name=ingredientListItem1]"
    );
    completedItem4.should("contain.text", "2 water");
    completedItem4.find("p.completed").should("not.exist");
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "2 ingredients to get"
    );
  });

  it("should add a new ingredient to the list with the form", () => {
    cy.visit("/ingredients");
    const ingredientAmountInput = cy.get("input[name=ingredientAmount]");
    const ingredientUnitInput = cy.get("input[name=ingredientUnit]");
    const ingredientNameInput = cy.get("input[name=ingredientName]");

    ingredientAmountInput.type("12");
    ingredientUnitInput.type("bags");
    ingredientNameInput.type("flour");
    cy.get("button[name=ingredientListFormAdd]").click();

    cy.get("app-ingredient-list-item", {}).should("have.length", 3);
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "3 ingredients to get"
    );
  });

  it("should remove the items from the list", () => {
    cy.visit("/ingredients");
    cy.get("button[name=removeIngredientListItem0]").click();
    cy.get("button[name=removeIngredientListItem0]").click();
    cy.get("button[name=removeIngredientListItem0]").click();

    cy.get("div[name=ingredientsEmptyMsg]").should("exist");
    cy.get("h5[name=ingredientListCount]").should(
      "contain",
      "0 ingredients to get"
    );
  });
});
