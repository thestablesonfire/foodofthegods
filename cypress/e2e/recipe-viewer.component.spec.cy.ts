describe("RecipeViewer", () => {
  beforeEach(() => {
    cy.login();
    cy.visit("/recipes");
    cy.get("app-recipe-list-item[recipeName=Test]").click();
  });

  it("should not be able to edit a recipe not created by the user", () => {
    cy.get("button[name=edit-recipe-button]").should("not.exist");
  });

  it("should double the ingredient amounts", () => {
    const recipeServingsInput = cy.get("input[name=recipe-servings]");
    recipeServingsInput.clear();
    recipeServingsInput.type("90");
    recipeServingsInput.blur();

    cy.get("li[name=ingredient0]").should("contain", "2 salt");
    cy.get("li[name=ingredient1]").should("contain", "2 water");
  });
});
